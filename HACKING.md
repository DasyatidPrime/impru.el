<!-- SPDX-License-Identifier: GPL-3.0-only -->

# Navigating this repository

Local standards are 99 columns for most code and documentation, but 76
columns for Markdown files and build system files. ‘.dir-locals.el’ is used
for per-mode settings, and file local variables are used for per-file
settings. Only ‘lexical-binding’ and ‘mode’ should be set in a file's first
line; other variables should be placed in trailing “Local variables” blocks.

All files that include nontrivial copyrightable work should have an
SPDX-License-Identifier in the file header. Most project-specific files are
placed under a GPL-3.0-only license, but most files related to the build
system (including most project-specific build scripts) are placed in the
public domain (via a “Creative Commons Public Domain Dedication and
Certification” aka CC-PDDC).

Note that Bitbucket doesn't handle Markdown with HTML in it, so Markdown
files intended to be viewed in a repository view (as opposed to intended
to be rendered and uploaded elsewhere, for instance) can't use dfn tags
or anything like that. Phooey.

More information later, perhaps.
