\input texinfo
@c %**start of header
@documentlanguage en
@documentencoding UTF-8
@setfilename impru.info
@settitle Imprū
@c %**end of header
@c SPDX-License-Identifier: GPL-3.0-only

@copying
@end copying

@titlepage
@title Imprū
@c @subtitle 
@author Robin Tarsiger

@page
@vskip 0pt plus1filll
@insertcopying
@end titlepage

@contents

@node Top
@top Imprū

@c XXX
This manual does not exist. If this manual begins to exist, seek help immediately.

@menu
* Introduction::
* Developer Notes::
@end menu

@node Introduction
@chapter Introduction

Imprū is an Emacs-based client for certain networked text-based virtual worlds, sometimes known as
MU*. It is primarily targeted at interacting with the line-oriented MUCK/MOO/MUSH families of
servers and is less likely to be suited to MUDs of the Aber, Diku, or LP style that make heavier
use of terminal features.

Right now, it's mostly a pile of code. You can actually get a
connection through with it though! I think!

Unless you're really interested in hacking on this, better wait until
there's actually some kind of announcement, perhaps.

@node Developer Notes
@appendix Developer Notes

This appendix includes notes on the design and implementation of Imprū. None of it forms any
kind of compatibility guarantee unless specifically marked otherwise.

@menu
* Coding conventions::
* Interaction buffer internals::
* Credentials internals::
* Why read-only auth-source::
* Internals warts::
@end menu

@node Coding conventions
@section Coding conventions

Some potentially idiosyncratic coding conventions are used in this project.

@menu
* Data structures conventions::
* Function name decorations::
* Variable name decorations::
@end menu

@node Data structures conventions
@subsection Data structures

A normal list owns its 1-deep cons structure and is copied using @code{copy-sequence}. A plist
follows the same ownership rules as a normal list. An alist most commonly owns its 2-deep cons
structure and is copied using @code{copy-alist}. An alist may also own the @code{cdr}s of its
entries if specified to do so, in which case a specialized copy function must be used.

Use a list for a sequence, an alist for a small homogeneous map, a plist with mostly-keyword keys
for an ‘open’ slotted structure, and a @code{cl-defstruct} type for a ‘closed’ slotted structure.

@node Function name decorations
@subsection Function name decorations

It is meaningful when a function's name ends (or in some cases begins) with any of the following
punctuation sequences:

@table @asis
@item @code{~} (tilde)
The function may assume it is being called in a backstage context in an interaction buffer. It
should thus not be called outside such a context.
@end table

@node Variable name decorations
@subsection Variable name decorations

It is meaningful when a variable's name ends (or in some cases begins) with any of the following
punctuation sequences:

@table @asis
@item @code{~} (tilde)
The variable is borrowing an otherwise mutable data structure to read it. Direct mutations on the
structure should not be performed while this variable is in use; instead, copies should be made as
needed. Think of this as similar to @code{&} in Rust.

@item @code{.} (dot)
The variable owns a unique copy of a data structure. The structure may be directly mutated through
this variable without interfering with other code. Note that this only applies to the ‘first-level’
structure and not to any elements contained in it, unless the data type in use specifies that they
are also owned.

@item @code{~.} (tildot)
The variable is borrowing a mutable data structure to mutate it, and the mutations may result in a
value that is not @code{eq} to the original value, in which case that value must be stored back
somewhere to avoid data corruption. This is not necessary for simple cases like heads of otherwise
immutable lists; it is primarily for alist and plist manipulation.
@end table

@node Interaction buffer internals
@section Interaction buffer internals

An interaction buffer is in the major mode @code{impru-interaction-mode}.

@menu
* Backstage context::
* Prompt and input internals::
* Process mark internals::
@end menu

@node Backstage context
@subsection Backstage context

Most of the time, most of the text in an interaction buffer is read-only from the user's
perspective. The user may also apply other edit-affecting states such as narrowing. From the
perspective of many Imprū functions, such as those that insert new output into the buffer, these
states must be bypassed. Thus a distinction is made between the normal user perspective on the
buffer and the perspective granted by a @dfn{backstage context}.

In a backstage context, narrowing is temporarily undone, and read-only text is made writable via
@code{inhibit-read-only}. The original value of point is also saved as with @code{save-excursion},
though code executing backstage may call @code{impru--leave-point-at} to move point from the user
perspective as well.

@node Prompt and input internals
@subsection Prompt and input

If an interaction buffer has an @emph{active prompt}, then @code{impru-prompt-overlay} is an
overlay covering the prompt, and the text category @code{impru-active-prompt} is applied to the
same text. Otherwise, it is @code{nil}. When the prompt is inactivated but left in the history, the
text is updated to have the category @code{impru-inactive-prompt}.

If an interaction buffer has an @emph{active input area}, then @code{impru-input-overlay} is an
overlay covering the input area, which is generally immediately after the prompt. Otherwise, it is
@code{nil}. The overlay sets a @code{field} of @code{impru-active-input} and is usually the only
area the user can type in. When the user commits an input line, the input text is updated to have
the category @code{impru-inactive-input}, and a newline is added at the end with the category
@code{impru-input-terminator} to force the field to end there. The way this works currently means
that the active input area @emph{must} be at the end of the buffer.

Note that the text category for the active prompt @emph{must} be a text category and not an overlay
category, because it must apply the @code{read-only} text property. Contrary to this, the overlay
category for the active input @emph{must} be an overlay category, because the overlay end must
advance to cover characters inserted on the input line even when there was no existing input, and
text properties can only be applied to characters that exist already.

The underlying text categories may be useful for finding earlier prompts and input in the buffer,
since the text is normally retained when input is sent. Note the difference in when the category is
set: it is set early for prompts and late for input.

Cases where one overlay exists but not the other probably shouldn't show up under normal
circumstances, but the buffer handling routines defensively treat these as semi-independent pieces
of state at present.

@node Process mark internals
@subsection The process mark

The process mark is associated in an Emacs-native fashion with the interaction process used by the
interaction buffer, but since we use a process filter, its actual handling is done in our code.
Notably, the process mark's insertion type should be treated as transient since it's convenient to
toggle it in some places, which is why when the process filter inserts new output, it sets the
insertion type specifically along the way.

Under most circumstances, the process mark is right behind the active prompt if there is one. When
this is true, functions that do things with the active prompt or input area should handle the mark
if necessary.

@node Credentials internals
@section Credential handling

@menu
* Password ref structure::
* Token structure::
@end menu

@node Password ref structure
@subsection Password refs

A login record in a worldspec doesn't have a property for a raw password; instead, it stores a
@dfn{password ref}. A password ref is a proper list whose @code{car} is a symbol designating a
@dfn{password ref function} capable of dereferencing it. Specifically, the function symbol should
have a valid @code{impru-password-ref-function-arity} property with its arity.

The arity of a password ref function is a cons of two integers. The @code{car} describes how many
context arguments (currently called ‘builtin’ arguments in the source) are included in the argument
list, and the @code{cdr} describes how many specific arguments are included. The two sublists are
concatenated to form the final argument list. Specific arguments are taken from the @code{cdr} of
the password ref, and context arguments are taken from the list @code{(@var{CONNSPEC})}, where
@var{CONNSPEC} is the connspec for the connection where a login is being attempted.

@node Token structure
@subsection Tokens

A @dfn{token} holds directly usable credentials. It is a plist with the following properties:

@table @code
@item :login
The login name to be used. Required.

@item :password
The raw password to be used. Required.

@item :save-function
A function of no arguments suitable for @code{impru-save-credentials-function}. When called,
interacts with the user to enable the use of these credentials persistently. See the documentation
for that variable for more details. Optional.
@end table

@node Why read-only auth-source
@section Why read-only auth-source

Why doesn't Imprū use the Emacs auth-source library for primary credential storage? This decision
was made on 2021-01-11 after observing the current behavior and limitations of auth-source from GNU
Emacs 27.2 on the author's system. (For documentation about auth-source itself,
@ref{Top,,,auth,Auth-source}.).

When the user deletes a world, any credentials that were stored with the world should not be
sitting around for an unknown period; nor should the user be asked to go dig in and find them
manually. However, the documentation for @code{auth-source-delete} states that “the backend may not
actually delete the entries”. Indeed, the @code{netrc} backend (which is configured by default)
silently does nothing, and the @code{secrets} backend signals an error specifically calling out the
functionality as unimplemented. So using auth-source as a primary store won't work.

There also appears to be some messiness or possible bugs in how the @code{:create} argument to
@code{auth-source-search} interacts with cached credentials and save functions, as well as some
weirdness with matching on host and port sometimes ignoring the username, but it's not necessary to
go into detail about those here.

So the primary managed credential store will be internal, with auth-source supported as a secondary
read-only backend.

Note that this description is a record of a design decision, and while it would be welcomed if the
auth-source maintainers chose to use this as inspiration for improvements, it is not meant as a
broader criticism of auth-source in itself.

@node Internals warts
@section Warts of structure and style

Aliases defined through the compatibility-switch mechanism are currently tagged with unconditional
autoload cookies. The normal autoload cookie doesn't expand the macro to see whether the
@code{defalias} will actually be there or not, and the mechanism doesn't seem customizable enough
to make this possible.

@bye
