;;; nattomaki.el --- plasmidic build code for elisp  -*- lexical-binding: t -*-

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Maintainer: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-21
;; SPDX-License-Identifier: CC-PDDC

;; All code in this file is in the public domain. If you add any copyrighted
;; code, remove this paragraph and the SPDX-License-Identifier above and add
;; copyright notices as needed. However, it is suggested that any changes be
;; given to the public domain as well. Share and enjoy!
;;
;; If you make changes, it is recommended to add to ‘nttmk-ancestry’ below,
;; add yourself as author, and set yourself as maintainer of the new file.

;;; Commentary:

;; This is a chunk of build code useful for making ELPA-able packages in a
;; way that isn't quite as... Web as some other build systems. “plasmidic”
;; in this context means that it is intended to be copied around between
;; repositories and ad-hoc variations made as necessary, instead of having
;; one true source of goodness. That's also why there's no version number.
;; See the block above the Commentary section in the source file regarding
;; making changes, and see the ‘nttmk-ancestry’ variable for information
;; on the lineage of this copy of this file.
;;
;; The name is due to the bacterially fermented nature of nattō and the
;; partial evocation of “make” by the word “maki”. Please forgive the lack
;; of ō in the file names.

;;; History:

;; ‘RTT-202201*’ -- 2021-01-21 -- Robin Tarsiger: Created.

;;; Code:

(require 'cl-lib)

;; Aside from the docstring: symbols should be added with the newest at the
;; top. Comments about the actual changes can go in the History section.
(defconst nttmk-ancestry
  '( RTT-202201 )
  "List of symbols indicating what's been done to this nattomaki.el.
These symbols are provided as subfeatures using ‘provide’ so that
Nattofiles can test for them. A symbol ending in ‘*’ means that
the related changes are marked as incomplete, causing the overall
version of Nattōmaki to be considered ‘volatile’; see the variable
‘nttmk-env-allow-volatile’.")

(defun nttmk-volatile-ancestry-p ()
  "Return non-nil if this version of Nattōmaki is volatile.
A volatile version of Nattōmaki is one that has any starred symbols
in ‘nttmk-ancestry’, which see."
  (cl-some (lambda (tag) (string-suffix-p "*" (symbol-name tag)))
           nttmk-ancestry))

(defconst nttmk-project-file-name "Nattofile"
  "Name of the elisp file with project-specific build information.
This file is located in the source directory.")

(defun nttmk-progress (format-string &rest args)
  "Show a progress string, as with ‘format’."
  (princ (apply #'format format-string args))
  (write-char ?\n))

(defun nttmk-expand-directory-name (name)
  "Expand NAME as with ‘expand-file-name’, but as a directory name."
  (file-name-as-directory (expand-file-name name)))

(defvar nttmk-source-directory nil
  "Absolute name of the source directory for this build.
This must be its name _as_ a directory and not as a file; that is,
in most circumstances it should end in a slash.")
(put 'nttmk-source-directory 'nttmk-variable-parse
     'nttmk-expand-directory-name)

(defvar nttmk-build-directory nil
  "Absolute name of the build directory for this build.
This must be its name _as_ a directory and not as a file; that is,
in most circumstances it should end in a slash. If this is the
same as the source directory, they should also be string-equal.")
(put 'nttmk-build-directory 'nttmk-variable-parse
     'nttmk-expand-directory-name)

(defvar nttmk-package-name nil
  "Name of the package being built.
Often initialized via ‘nttmk-set-package-info-from’.")
(put 'nttmk-package-name 'nttmk-variable-parse 'identity)

(defvar nttmk-package-version nil
  "Version of the package being built, as a string.
Often initialized via ‘nttmk-set-package-info-from’.")
(put 'nttmk-package-version 'nttmk-variable-parse 'identity)

(defun nttmk-building-in-source-p ()
  "Return non-nil if the build and source directories are the same.
This currently just compares them for string equality."
  (string= nttmk-build-directory nttmk-source-directory))

(defun nttmk-delete-build-file (file)
  "Delete FILE relative to the build directory if it exists.
FILE must be a relative file name."
  (let ((build-file (concat nttmk-build-directory file)))
    (when (file-exists-p build-file)
      (nttmk-progress "Deleting %s" build-file)
      (delete-file build-file))))

(defun nttmk-call-process (program &rest args)
  "Call PROGRAM with ARGS, signaling an error if it fails.
Anything other than a zero exit status is considered a failure.
The output from PROGRAM is captured in a temporary buffer and
printed afterwards."
  (with-temp-buffer
    (let ((exit-status (apply #'call-process program nil t nil args)))
      (princ (buffer-substring (point-min) (point-max)))
      (unless (equal exit-status 0)
        (error "%s exited with failure: %s" program exit-status)))))

(defun nttmk-set-package-info-from (elisp-file)
  "Set global package variables based on the source ELISP-FILE.
ELISP-FILE must be relative to the source directory."
  (require 'lisp-mnt)
  (let ((source-file (concat nttmk-source-directory elisp-file)))
    (setq nttmk-package-name (file-name-sans-extension
                               (file-name-nondirectory source-file))
          nttmk-package-version (or (lm-version source-file)
                                     (error "Can't extract version from %s"
                                            source-file)))))

(defun nttmk-write-package-desc-file (desc-file elisp-file)
  "Write DESC-FILE with a ‘define-package’ form based on ELISP-FILE.
DESC-FILE must be relative to the build directory.
ELISP-FILE must be relative to the source directory."
  (require 'package)
  (let* ((source-file (concat nttmk-source-directory elisp-file))
         (build-desc-file (concat nttmk-build-directory desc-file)))
    (when (file-newer-than-file-p source-file build-desc-file)
      (let* ((source-buffer (find-file-noselect source-file))
             (desc (with-current-buffer source-buffer
                     (package-buffer-info))))
        (package-generate-description-file desc build-desc-file)
        (nttmk-progress "Wrote %s" build-desc-file)))))

(defun nttmk-byte-compile (elisp-file &optional force load)
  "Byte-compile ELISP-FILE into the build directory.
ELISP-FILE must be relative to the source directory. A copy of
ELISP-FILE itself will also wind up in the build directory if
it is not the same as the source directory. FORCE and LOAD
are interpreted as for ‘byte-recompile-file’."
  (require 'bytecomp)
  (let ((source-el (concat nttmk-source-directory elisp-file))
        (build-el (concat nttmk-build-directory elisp-file)))
    (when (and (not (nttmk-building-in-source-p))
               (file-newer-than-file-p source-el build-el))
      (copy-file source-el build-el t t))
    ;; The byte-compiler already outputs a message.
    ;;(nttmk-progress "Compiling %s" (concat build-el "c"))
    (byte-recompile-file build-el force 0 load)))

(defun nttmk-call-makeinfo (info-file &optional texi-file)
  "Create an Info file from a Texinfo file.
INFO-FILE must be relative to the build directory. TEXI-FILE must
be relative to the source directory if provided; otherwise, it is
inferred from INFO-FILE. This just calls the external ‘makeinfo’
program."
  (unless texi-file
    (setq texi-file (replace-regexp-in-string "\\.info\\'" ".texi"
                                              info-file)))
  (let ((build-info (concat nttmk-build-directory info-file))
        (source-texi (concat nttmk-source-directory texi-file)))
    (when (file-newer-than-file-p source-texi build-info)
      (nttmk-progress "Rendering Info file %s" build-info)
      (nttmk-call-process "makeinfo" "--output" build-info source-texi))))

(defun nttmk--insert-tar-octal (field-bytes number)
  "Insert a tar header field FIELD-BYTES long with the octal number NUMBER."
  (let ((ascii (format "%o" number)))
    (unless (<= (length ascii) (1- field-bytes))
      (error "Octal number too long for this field: %o (%d)"
             number field-bytes))
    (insert (make-string (- field-bytes 1 (length ascii)) ?0)
            ascii 0)))

(defun nttmk--insert-tar-ascii (field-bytes string)
  "Insert a tar header field FIELD-BYTES long with the string STRING."
  (unless (<= (length string) (1- field-bytes))
    (error "String too long for this field: %S (%d)" string field-bytes))
  (insert string (make-string (- field-bytes (length string)) 0)))

(cl-defun nttmk--insert-tar-member
    (file-name data-buffer &key modification-time directory executable)
  "Insert a new tar member into the current buffer.
FILE-NAME is the name of the member itself. If DATA-BUFFER is
non-nil, it is a binary buffer containing the data for the file.

The resultant member uses perfunctory values for UID, GID, and
permission bits, assuming it will be part of an archive for
distribution and eventually extracted into an unknown context
where all of those will be reset anyway. The header format used
is similar to the output of “pax -x ustar”, though splitting the
file name to use the extended length is not supported.

If MODIFICATION-TIME is provided, it becomes the modification
time of the tar member. Otherwise, the current time is used.

If DIRECTORY is non-nil, a directory member is written, and
DATA-BUFFER should not be provided. Otherwise, a regular file
member is written. If EXECUTABLE is non-nil, the file is marked
as executable; otherwise, it is not."
  (let ((binary-file-name (encode-coding-string file-name 'utf-8 t)))
    (unless (<= (length binary-file-name) 100)
      (error "File name too long for basic tar format: %s" file-name))
    ;; Some sources include type bits in the mode, but not all.
    (let ((block-size 512)
          (mode (if (or directory executable #o755) #o644))
          (byte-size (if data-buffer (buffer-size data-buffer) 0))
          (mtime-int (time-convert (or modification-time (current-time))
                                   'integer)))
      (let ((header-start (point)) checksum-pos header-end)
        ;; Initial part of tar header.
        (nttmk--insert-tar-ascii 100 binary-file-name)
        ;; File mode, which seemingly includes some type bits.
        (nttmk--insert-tar-octal 8 mode)
        (nttmk--insert-tar-octal 8 65534)         ; UID
        (nttmk--insert-tar-octal 8 65534)         ; GID
        (nttmk--insert-tar-octal 12 byte-size)
        (nttmk--insert-tar-octal 12 mtime-int)
        ;; Temporary checksum.
        (setq checksum-pos (point))
        (insert (make-string 8 #x20))
        ;; UStar section.
        (insert (if directory ?5 ?0)
                (make-string 100 0)               ; link target
                "ustar\0" "00")
        (nttmk--insert-tar-ascii 32 "nobody")     ; user name
        (nttmk--insert-tar-ascii 32 "nobody")     ; group name
        (insert (make-string 8 0)                 ; device major
                (make-string 8 0)                 ; device minor
                (make-string 155 0)               ; filename prefix
                "")
        ;; Padding.
        (insert (make-string (- (+ block-size header-start) (point)) 0))
        (setq header-end (point))
        ;; Calculate and replace checksum.
        (let ((checksum 0))
          (save-excursion
            (goto-char header-start)
            (while (< (point) header-end)
              (setq checksum (+ checksum (following-char)))
              (forward-char 1))
            (goto-char checksum-pos)
            (delete-region (point) (+ 8 (point)))
            (nttmk--insert-tar-octal 8 checksum)))
        (when data-buffer
          (let ((data-start (point)))
            (insert-buffer-substring data-buffer)
            ;; Pad to block size.
            (let ((overhang-bytes (mod (- (point) data-start) block-size)))
              (unless (zerop overhang-bytes)
                (insert (make-string (- block-size overhang-bytes) 0))))))))))

(defun nttmk--insert-tar-trailer ()
  "Insert a tar end-of-file trailing into the current buffer."
  (insert (make-string 1024 0)))

(defun nttmk-build-package-archive (files)
  "Build a package distribution archive containing FILES.
FILES is a list of file names, all of which must be relative to
the build directory and which must not contain any directory
components (forming a flat structure). If FILES is appropriately
set up, the resultant archive file is a compressed tar file of
the form that ‘package.el’ accepts (except possibly for the
compression).

The archive is named “NAME-VERSION.tar.gz”, with NAME and VERSION
taken from ‘nttmk-package-name’ and ‘nttmk-package-version’
respectively. Files are read from the build directory (per above)
and implicitly placed into a directory named “NAME-VERSION” in
the archive.

For ‘package.el’ format, FILES should contain Emacs Lisp source
files as well as a package description (-pkg.el) file of the kind
generated by ‘nttmk-write-package-desc-file’. It may also contain
Info files and/or a ‘dir’ file, but it should not contain compiled
Lisp files or generated autoload files."
  (require 'lisp-mnt)
  (let* ((package-name (or nttmk-package-name
                           (error "Package name is unset")))
         (version-string (or nttmk-package-version
                             (error "Version is unset")))
         (content-name (concat package-name "-" version-string))
         (build-tar-file (concat nttmk-build-directory
                                 content-name ".tar")))
    (with-temp-buffer
      (setq-local buffer-file-coding-system 'no-conversion)
      (set-buffer-multibyte nil)
      (nttmk--insert-tar-member (concat content-name "/") nil :directory t)
      (dolist (file (sort (copy-sequence files) #'string<))
        (let* ((actual-file (concat nttmk-build-directory file))
               (attrs (or (file-attributes actual-file)
                          (error "Can't find file for archive: %s"
                                 actual-file)))
               (mtime (file-attribute-modification-time attrs))
               (data-buffer (find-file-noselect actual-file nil t))
               (member-name (concat content-name "/" file)))
          (nttmk--insert-tar-member member-name data-buffer
                                     :modification-time mtime)))
      (nttmk--insert-tar-trailer)
      (write-region nil nil build-tar-file))
    (let ((compressed-file (concat build-tar-file ".gz")))
      (when (file-exists-p compressed-file)
        (delete-file compressed-file))
      (nttmk-call-process "gzip" "--" build-tar-file)
      (nttmk-progress "Wrote %s" compressed-file))))

(defconst nttmk-env-allow-volatile "NATTOMAKI_ALLOW_VOLATILE"
  "Environment variable for allowing volatile versions of Nattōmaki to run.
If this environment variable is unset or empty, and if there are
any starred entries in ‘nttmk-ancestry’ indicating that
incomplete changes have been made to this file, then the
Nattōmaki CLI interface will abort rather than performing any
actions.")

(defun nttmk-cli-run-1 (args)
  "Execute Nattōmaki CLI actions based on ARGS.
ARGS consists of any number of variable overrides, a recipe name,
and then possible further arguments to the recipe. This is mainly
called by ‘nttmk-cli-run’, which see.

The Nattofile in the _current_ directory is read before anything
else happens. Note that a “source-directory” override via the CLI
will be applied _after_ the Nattofile is read.

Variable overrides are of the form VAR=VALUE. The variable symbol
affected is VAR prefixed with “nttmk-”, and the string value is
passed through the function designated by the variable's
‘nttmk-variable-parse’ property to obtain the actual value. If
the property is unset, the variable cannot be set through the
CLI. The “build-directory” (that is, ‘nttmk-build-directory’
variable is particularly relevant.

The recipe name is used to search for a function by that name
prefixed with “nttmk/” whose ‘nttmk-recipe’ function property is
set. Recipe functions are usually defined in the Nattofile. The
function is then called with all the remaining string arguments
as its arguments.

See ‘nttmk-env-allow-volatile’ for the behavior of this function
if there are any starred entries in ‘nttmk-ancestry’."
  (let ((nttmk-source-directory (nttmk-expand-directory-name "."))
        (nttmk-build-directory (nttmk-expand-directory-name ".")))
    (when (and (nttmk-volatile-ancestry-p)
               (member (getenv nttmk-env-allow-volatile) '(nil "")))
      (error "This is a volatile version of nattomaki.el; set %s=1 to use"
             nttmk-env-allow-volatile))
    (let ((project-file (concat nttmk-source-directory
                                nttmk-project-file-name)))
      (unless (file-exists-p project-file)
        (error "No project file found; should be %s" project-file))
      (load project-file nil t t))
    (while (string-match "\\`\\([a-z][-a-z0-9]*\\)=" (car args))
      (let* ((var-short-name (match-string 1 (car args)))
             (var (intern (format "nttmk-%s" var-short-name)))
             (intended-value (substring (car args) (match-end 0))))
        (unless (get var 'nttmk-variable-parse)
          (error "Don't know how to set variable ‘%s’" var-short-name))
        (set var (funcall (get var 'nttmk-variable-parse) intended-value)))
      (setq args (cdr args)))
    (when (string-match "=" (car args))
      (error "Bad variable name ‘%s’"
             (substring (car args) 0 (match-beginning 0))))
    (let ((load-path (cons nttmk-build-directory load-path)))
      (let* ((recipe-short-name (car args))
             (recipe-fn (intern (format "nttmk/%s" recipe-short-name))))
        (unless (function-get recipe-fn 'nttmk-recipe)
          (error "Unknown recipe ‘%s’" recipe-short-name))
        (apply recipe-fn (cdr args))))))

(defun nttmk-cli-run ()
  "Execute Nattōmaki CLI actions and return an exit code.
This searches for the first \"--\" entry in ‘command-line-args’
and takes as CLI arguments every argument after that. The exit
code returned is zero for success and nonzero for failure; Lisp
errors are caught and turned into a message and exit code."
  (condition-case err
      (prog1 0
        (nttmk-cli-run-1 (cdr (member "--" command-line-args))))
    (error
     (prog1 1 (message "%s" (error-message-string err))))))

(provide 'nattomaki nttmk-ancestry)

;; Local variables:
;; fill-column: 76
;; End:

;;; nattomaki.el ends here
