;;; impru-mcp.el --- MCP support for Imprū                               -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-08
;; Version: 0.2.0
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:



;;; Code:

(require 'cl-lib)
(require 'seq)
(require 'impru-protocol)

(cl-defstruct (impru-mcp-package
               (:constructor make-impru-mcp-package (name min-version max-version)))
  (name nil) ; string
  (min-version nil)
  (max-version nil)
  )

(cl-defstruct (impru-mcp-known-package
               (:constructor make-impru-mcp-known-package
                             (name min-version max-version
                                   activate-function handler-function))
               (:include impru-mcp-package))
  activate-function
  handler-function
  )

(cl-defstruct impru-mcp-state
  (version nil)
  (authenticator nil)
  (encode-line-buf "") ; nil if passthrough to eol
  (decode-line-buf "") ; nil if passthrough to eol
  (pending-multiline '())
  (active-package-state-table (make-hash-table :test #'equal :size 10))
  (server-packages '())
  )

(defvar impru-mcp-known-package-table (make-hash-table :test #'equal :size 10))

(defun impru-mcp-negotiate--activate (mcp-state _package-name _chosen-version)
  t)

;; TODO: mooooovvvvee fffffff
(defun impru-mcp--format (mcp-state message-type &optional arguments)
  (with-output-to-string
    (princ (format "#$#%s %s" message-type (impru-mcp-state-authenticator mcp-state)))
    (let ((multilines '()))
      ;; TODO: when do we or do we not automatically split outgoing arguments on lines?
      (dolist (entry arguments)
        (cond
         ((null (cddr entry))                     ; Single-line argument.
          (princ (format " %s: " (car entry)))    ; XXX check the name? or just leave it?
          (let ((value (cadr entry)))
            (if (string-match (concat "\\`" impru-mcp--re-unquoted-string "\\'") value)
                (princ value)
              (write-char ?\")
              ;; Note that the spec grammar specifically only allows these two chars to be quoted.
              (princ (replace-regexp-in-string "\\([\\\"]\\)" "\\\\\\1" value t))
              (write-char ?\"))))
         (t                                       ; Multi-line argument.
          ;; XXX: I wonder how many people check for exactly ""
          (princ (format " %s*: _" (car entry)))
          (push entry multilines))))
      (if (null multilines)
          (write-char ?\n)
        (let ((data-tag (impru-mcp--choose-data-tag)))
          (princ (format " _data-tag: %s\n" data-tag))
          (dolist (entry (setq multilines (nreverse multilines)))
            (let ((prefix (format "#$#* %s %s: " data-tag (car entry))))
              (dolist (line (cdr entry))
                (princ prefix)
                (princ line)
                (write-char ?\n))))
          (princ (format "#$#: %s\n" data-tag)))))))

(defun impru-mcp-negotiate--send-known-package-list (mcp-state reply)
  (maphash
   (lambda (_ package)
     (let ((args `((package ,(impru-mcp-package-name package))
                   (min-version ,(impru-mcp-format-version
                                  (impru-mcp-package-min-version package)))
                   (max-version ,(impru-mcp-format-version
                                  (impru-mcp-package-max-version package))))))
       (funcall reply (impru-mcp--format mcp-state "mcp-negotiate-can" args))))
   impru-mcp-known-package-table)
  (funcall reply (impru-mcp--format mcp-state "mcp-negotiate-end")))

(defun impru-mcp-negotiate--handle-message (mcp-state message-type arguments reply _forward)
  (when (string-match "-\\([^-]+\\)\\'" message-type)
    (cl-case (intern-soft (downcase (match-string 1 message-type)))
      (can
       (when-let ((name (cadr (assq 'package arguments)))
                  (min-version (impru-mcp-parse-version (cadr (assq 'min-version arguments))))
                  (max-version (impru-mcp-parse-version (cadr (assq 'max-version arguments)))))
         ;; TODO: Fuzzball and Proto keep sending these. Want to track whether we've seen an
         ;; -end and do immediate activations thereafter.
         (push (make-impru-mcp-package (downcase name) min-version max-version)
               (impru-mcp-state-server-packages mcp-state))))
      (end
       (dolist (wanted (impru-mcp-state-server-packages mcp-state))
         (when-let ((name (impru-mcp-package-name wanted))
                    (known (gethash name impru-mcp-known-package-table))
                    (chosen-version
                     (impru-mcp-choose-version
                      (impru-mcp-package-min-version wanted)
                      (impru-mcp-package-max-version wanted)
                      (impru-mcp-package-min-version known)
                      (impru-mcp-package-max-version known)))
                    (package-state
                     (funcall (impru-mcp-known-package-activate-function known)
                              mcp-state name chosen-version)))
           ;; Do not use this for sending any -can messages. The MCP 2.1 specification §3.1
           ;; specifically disallows waiting until an -end message to send your -can messages.
           (puthash name package-state
                    (impru-mcp-state-active-package-state-table mcp-state)))))
      (otherwise
       ;; TODO: this is too warny, want more trace levels
       (impru-mcp--protocol-warning
        state "Unknown mcp-negotiate message: %s" message-type)))))

(puthash "mcp-negotiate"
         (make-impru-mcp-known-package
          "mcp-negotiate" '(1 0) '(2 0)
          #'impru-mcp-negotiate--activate
          #'impru-mcp-negotiate--handle-message)
         impru-mcp-known-package-table)



(defconst impru-mcp--re-simple-char-body
  "][~`!@#$%^&()=+{}|';?/><.,A-Za-z0-9-")
(defconst impru-mcp--re-simple-char
  ;; This is always one character class.
  (concat "[" impru-mcp--re-simple-char-body "]"))
(defconst impru-mcp--re-unquoted-string
  (concat impru-mcp--re-simple-char "+"))
(defconst impru-mcp--re-ident
  "[A-Za-z][-A-Za-z0-9]+")
(defconst impru-mcp--re-message-start
  (concat "\\`\\(" impru-mcp--re-ident "\\)"
          " +\\(" impru-mcp--re-unquoted-string "\\)"))
(defconst impru-mcp--re-message-continue
  (concat "\\`\\* +\\(" impru-mcp--re-unquoted-string "\\)"
          " +\\(" impru-mcp--re-ident "\\): "))
(defconst impru-mcp--re-message-end
  ;; We're slightly forgiving with the whitespace here...
  (concat "\\`: +\\(" impru-mcp--re-unquoted-string "\\) *\\'"))
(defconst impru-mcp--re-quoted-string
  (concat "\"\\(?:" impru-mcp--re-simple-char "\\|[*: ]\\|\\\\[\\\"]\\)*\""))
(defconst impru-mcp--re-keyval
  (concat " +\\(" impru-mcp--re-ident "\\)\\(\\*\\)?:"
          " +\\(?:\\(" impru-mcp--re-unquoted-string "\\)"
          "\\|\\(" impru-mcp--re-quoted-string "\\)\\)"))

(defun impru-mcp--protocol-warning (state format-string &rest args)
  (apply #'warn format-string args))

(defconst impru-mcp-protocol-min-version '(2 1))
(defconst impru-mcp-protocol-max-version '(2 1))

(defun impru-mcp-format-version (version)
  (if (and (consp version) (integerp (car version))
           (consp (cdr version)) (integerp (cadr version)))
      (format "%d.%d" (car version) (cadr version))
    "<unknown>"))

(defun impru-mcp-parse-version (thing)
  (save-match-data
    (when (and (stringp thing) (string-match "\\`\\([0-9]+\\)\\.\\([0-9]+\\)\\'" thing))
      (list (string-to-number (match-string 1 thing))
            (string-to-number (match-string 2 thing))))))

(defun impru-mcp-choose-version (server-min server-max client-min client-max)
  (and server-min server-max client-min client-max
       (version-list-<= server-min client-max)
       (version-list-<= client-min server-max)
       (if (version-list-<= server-max client-max) server-max client-max)))

(defun impru-mcp-intern (string)
  (or (intern-soft string) string))

(defun impru-mcp--parse-kvs (string &optional start)
  (catch 'return
    (let ((pos (or start 0)) (alist '()))
      (while (< pos (length string))
        (unless (equal pos (string-match impru-mcp--re-keyval string pos))
          (throw 'return '(error)))
        (setq pos (match-end 0))
        (let ((key-string (match-string 1 string))
              (multiline (match-string 2 string))
              (unquoted-val (match-string 3 string))
              (quoted-val (match-string 4 string)))
          (let ((parsed-val
                 (cond
                  (multiline '*)
                  (unquoted-val unquoted-val)
                  (quoted-val
                   (replace-regexp-in-string
                    "\\\\\\(.\\)" "\\1" (substring quoted-val 1 (1- (length quoted-val)))))
                  ;; Shouldn't happen, but...
                  (t (throw 'return '(error))))))
            (push (list (impru-mcp-intern (downcase key-string)) parsed-val) alist))))
      (nreverse alist))))

(defun impru-mcp--choose-token ()
  (let ((alphabet "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        (string (make-string 16 0)))
    ;; TODO: more secure randomness...
    (dotimes (i (length string))
      (aset string i (aref alphabet (random (length alphabet)))))
    string))

(defalias 'impru-mcp--choose-authenticator 'impru-mcp--choose-token)
(defalias 'impru-mcp--choose-data-tag 'impru-mcp--choose-token)

(defun impru-find-handler (state message-type)
  (save-match-data
    (catch 'found
      ;; message-type is already downcased.
      (let ((prefix message-type))
        (while t
          (when-let ((package (gethash prefix impru-mcp-known-package-table)))
            (throw 'found (impru-mcp-known-package-handler-function package)))
          (if (string-match "-[^-]+\\'" prefix)
              (setq prefix (substring prefix 0 (match-beginning 0)))
            (throw 'found nil)))))))

(defun impru-mcp-parse-incoming-line (state string bol after-eol reply forward)
  (let ((before-eol after-eol))
    (when (and (< bol before-eol) (= (aref string (1- before-eol)) ?\n))
      (setq before-eol (1- before-eol)))
    (when (and (< bol before-eol) (= (aref string (1- before-eol)) ?\r))
      (setq before-eol (1- before-eol)))
    (if (< (- before-eol bol) 3)
        (impru-mcp--protocol-warning state "Internal error mcp-100")
      (let ((line-data (substring string (+ bol 3) before-eol)))
        (cond
         ((string-match "\\`\\(mcp\\) +" line-data)
          (let* ((init-kv (impru-mcp--parse-kvs line-data (match-end 1)))
                 (version-low (impru-mcp-parse-version (cadr (assq 'version init-kv))))
                 (version-high (impru-mcp-parse-version (cadr (assq 'to init-kv))))
                 (version (impru-mcp-choose-version
                           version-low version-high
                           impru-mcp-protocol-min-version
                           impru-mcp-protocol-max-version)))
            (if (null version)
                (impru-mcp--protocol-warning
                 state "No MCP version overlap (server %s to %s, client %s to %s)"
                 (impru-mcp-format-version version-low)
                 (impru-mcp-format-version version-high)
                 (impru-mcp-format-version
                  impru-mcp-protocol-min-version)
                 (impru-mcp-format-version
                  impru-mcp-protocol-max-version))
              (let* ((authenticator (impru-mcp--choose-authenticator))
                     (hello-line
                      (format "#$#mcp authentication-key: %s version: %s to: %s\n"
                              authenticator
                              (impru-mcp-format-version
                               impru-mcp-protocol-min-version)
                              (impru-mcp-format-version
                               impru-mcp-protocol-max-version))))
                (setf (impru-mcp-state-version state) version
                      (impru-mcp-state-authenticator state) authenticator)
                (funcall reply hello-line)
                ;; TODO: sth sth anti-fingerprinting? anti-etc.? But...
                (impru-mcp-negotiate--send-known-package-list state reply)))))
         ((null (impru-mcp-state-version state))
          (funcall forward string bol after-eol))
         ((string-match impru-mcp--re-message-start line-data)
          (let* ((message-type (match-string 1 line-data))
                 (handler (impru-find-handler state (downcase message-type)))
                 (authenticator (match-string 2 line-data)))
            (cond
             ((null handler) nil)
             ((not (equal authenticator (impru-mcp-state-authenticator state)))
              ;; TODO: ... do we need constant time comparison?
              (impru-mcp--protocol-warning state "Bad authenticator, dropping message")) 
             (t
              (let ((arguments (impru-mcp--parse-kvs line-data (match-end 0))))
                (cond
                 ((eq 'error (car arguments))
                  (impru-mcp--protocol-warning state "Bad keyval syntax, dropping message"))
                 ((seq-some (lambda (entry) (eq '* (cadr entry))) arguments)
                  (if-let ((data-tag (cadr (assq '_data-tag arguments))))
                      ;; TODO: resource limits?
                      (push (cons data-tag (cons message-type arguments))
                            (impru-mcp-state-pending-multiline state))
                    (impru-mcp--protocol-warning state "Bad multiline start, dropping message")))
                 (t
                  (funcall handler state message-type arguments reply forward))))))))
         ((string-match impru-mcp--re-message-continue line-data)
          ;; Silently ignore unmatched continuation lines; we might've dropped the start of the
          ;; message due to not supporting it.
          (when-let ((tag (match-string 1 line-data))
                     (message-record (assoc tag (impru-mcp-state-pending-multiline state)))
                     (key (impru-mcp-intern (match-string 2 line-data)))
                     (key-entry (assoc key (cddr message-record))))
            (when (eq '* (cadr key-entry))
              (setcdr (cdr key-entry)
                      (cons (substring line-data (match-end 0)) (cddr key-entry))))))
         ((string-match impru-mcp--re-message-end line-data)
          ;; Silently ignore unmatched end lines, similarly to above.
          (when-let ((tag (match-string 1 line-data))
                     (message-record (assoc tag (impru-mcp-state-pending-multiline state)))
                     (message-type (cadr message-record))
                     (handler (impru-find-handler message-type)))
            (let ((arguments (cddr message-record)))
              ;; We own the argument alist.
              (dolist (entry arguments)
                (when (eq '* (cadr entry))
                  (setcdr entry (cddr entry))))
              (funcall handler state message-type arguments reply forward))))
         (t
          (impru-mcp--protocol-warning state "Dropping strange line")))))))

(defun impru-mcp--encode (state chars start end reply forward)
  (if (not (stringp chars))
      (funcall forward chars start end)
    (let ((pos start))
      (while (< pos end)
        (cond
         ((null (impru-mcp-state-encode-line-buf state))
          (let ((probe pos) (after-eol nil))
            (while (and (not after-eol) (< probe end))
              (setq after-eol (= (aref chars probe) ?\n)
                    probe (1+ probe)))
            (funcall forward chars pos probe)
            (setq pos probe)
            (when after-eol
              (setf (impru-mcp-state-encode-line-buf state) ""))))
         (t
          (let* ((len-to-add (min 3 (- end pos)))
                 (chars-to-add (substring chars pos (+ pos len-to-add)))
                 (line-thus-far (concat (impru-mcp-state-encode-line-buf state)
                                        chars-to-add)))
            (setq pos (+ pos len-to-add))
            (cond
             ((< (length line-thus-far) 3)
              (if-let ((nl-position (seq-position line-thus-far ?\n)))
                  (progn
                    (funcall forward line-thus-far 0 (1+ nl-position))
                    (setf (impru-mcp-state-encode-line-buf state)
                          (substring line-thus-far (1+ nl-position))))
                (setf (impru-mcp-state-encode-line-buf state) line-thus-far)))
             ((string-prefix-p "#$" line-thus-far)
              (funcall forward (if (impru-mcp-state-version state)
                                   (concat "#$\"" line-thus-far)
                                 line-thus-far))
              (setf (impru-mcp-state-encode-line-buf state) nil))
             (t
              (funcall forward line-thus-far)
              (setf (impru-mcp-state-encode-line-buf state) nil))))))))))

(defun impru-mcp--decode (state chars start end reply forward)
  (if (not (stringp chars))
      (funcall forward chars start end)
    (let ((pos start))
      (while (< pos end)
        (cond
         ((null (impru-mcp-state-decode-line-buf state))
          ;; TODO: duplication with below
          (let ((probe pos) (after-eol nil))
            (while (and (not after-eol) (< probe end))
              (setq after-eol (= (aref chars probe) ?\n)
                    probe (1+ probe)))
            (funcall forward chars pos probe)
            (setq pos probe)
            (when after-eol
              (setf (impru-mcp-state-decode-line-buf state) ""))))
         (t
          ;; TODO: duplication with above
          (let* ((len-to-add (min 3 (- end pos)))
                 (chars-to-add (substring chars pos (+ pos len-to-add)))
                 (line-thus-far (concat (impru-mcp-state-decode-line-buf state)
                                        chars-to-add)))
            (setq pos (+ pos len-to-add))
            (cond
             ((< (length line-thus-far) 3)
              (if-let ((nl-position (seq-position line-thus-far ?\n)))
                  (progn
                    (funcall forward line-thus-far 0 (1+ nl-position))
                    (setf (impru-mcp-state-decode-line-buf state)
                          (substring line-thus-far (1+ nl-position))))
                (setf (impru-mcp-state-decode-line-buf state) line-thus-far)))
             ((string-prefix-p "#$#" line-thus-far)
              (if-let ((nl-position (seq-position line-thus-far ?\n)))
                  (progn
                    ;; Have to include the newline and prefix in case it needs to be forwarded.
                    ;; Can't do the version check on the outside because the startup line looks
                    ;; like this too.
                    (impru-mcp-parse-incoming-line
                     state line-thus-far 0 (1+ nl-position) reply forward)
                    (setf (impru-mcp-state-decode-line-buf state)
                          (substring line-thus-far (1+ nl-position))))
                ;; TODO: line length limits
                (setf (impru-mcp-state-decode-line-buf state) line-thus-far)))
             ((string-prefix-p "#$\"" line-thus-far)
              (funcall forward (if (impru-mcp-state-version state)
                                   (substring line-thus-far 3)
                                 line-thus-far))
              (setf (impru-mcp-state-decode-line-buf state) nil))
             (t
              (funcall forward line-thus-far)
              (setf (impru-mcp-state-decode-line-buf state) nil))))))))))

(impru-protocol-define-layer-class
 'mcp
 #'make-impru-mcp-state
 #'impru-mcp--encode
 #'impru-mcp--decode)

(provide 'impru-mcp)

;;; impru-mcp.el ends here
