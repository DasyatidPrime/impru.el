;; SPDX-License-Identifier: CC-PDDC

;; Commented-out evals are recommended minor modes. Set them yourself in .dir-locals-2.el if you
;; want them.
((emacs-lisp-mode
  ;;(eval . (outline-minor-mode +1))
  (comment-column . 50)
  (fill-column . 99)
  (outline-regexp . ";;;;* +[^ \n]"))
 (texinfo-mode
  ;;(eval . (auto-fill-mode +1))
  (fill-column . 99))
 (markdown-mode
  (fill-column . 76)))
