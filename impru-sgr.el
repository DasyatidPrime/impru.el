;;; impru-sgr.el --- ANSI SGR formatting for Imprū                       -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-09
;; Version: 0.2.0
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:



;;; Code:


(require 'cl-lib)
(require 'impru-protocol)

(defvar impru-sgr-palette nil)

(defun impru-sgr-make-default-palette ()
  (vconcat
   [#x000000 #xcd0000 #x00cd00 #xcdcd00 #x0000ee #xcd00cd #x00cdcd #xe5e5e5]
   [#x7f7f7f #xff0000 #x00ff00 #xffff00 #x5c5cff #xff00ff #x00ffff #xffffff]
   (let ((ramp [#x00 #x5f #x87 #xaf #xd7 #xff])
         (cube (make-vector (* 6 6 6) 0)))
     (prog1 cube
       (dotimes (r 6)
         (dotimes (g 6)
           (dotimes (b 6)
             (aset cube (+ b (* 6 g) (* 36 r))
                   (+ (aref ramp b)
                      (ash (aref ramp g) 8)
                      (ash (aref ramp r) 16))))))))
   (let ((gray (make-vector 24 0)))
     (prog1 gray
       (dotimes (i 24)
         (aset gray i (* #x010101 (+ 8 (* 10 i)))))))))

(cl-defstruct (impru-sgr-state
               (:constructor make-impru-sgr-state ()))
  (after-esc nil)
  (attrs '())
)

(defconst impru-sgr--attr-fg #xff)
(defconst impru-sgr--attr-bg (ash #xff 8))
(defconst impru-sgr--attr-bold (ash 1 16))
(defconst impru-sgr--attr-reverse (ash 1 17))
(defconst impru-sgr--attr-underline (ash 1 18))

(defconst impru-sgr--max-csi-length 32) ; after ESC

(defun impru-sgr--csi-done-p (buf start end)
  (not
   (or (= end start)
       ;; Continue after question mark immediately following.
       (and (= end (1+ start))
            (= (aref buf start) ??))
       ;; Continue after digits or semicolon.
       (let ((last (aref buf (1- end))))
         (or (<= ?0 last ?9) (= last ?\;))))))

(defun impru-sgr-get-color (index)
  (let ((palette (or impru-sgr-palette
                     (setq impru-sgr-palette (impru-sgr-make-default-palette)))))
    (and (<= 0 index (1- (length palette)))
         (let ((color (aref palette index)))
           (cond
            ((stringp color) color)
            ((integerp color) (format "#%06x" color))
            ;; Give up and squash strange colors. XXX
            (t nil))))))

(defun impru-sgr--process-csi (state buf start end forward)
  (when (< start end)
    (let* ((op-char (aref buf (1- end)))
           (param-str (substring buf start (1- end)))
           (parameters
            (if (string-empty-p param-str) '()
              ;; Don't set omit-nulls here; those are properly treated as zero. I think.
              (mapcar #'string-to-number (split-string param-str ";")))))
      (when (= ?m op-char)
        ;; A proper SGR!
        (let ((attrs (impru-sgr-state-attrs state)) p)
          (while parameters
            (cl-case (setq p (pop parameters))
              (0 (setq attrs '()))
              (1 (setq attrs (plist-put attrs :weight 'bold)))
              ((21 22) (setq attrs (plist-put attrs :weight 'normal)))
              (7 (setq attrs (plist-put attrs :inverse-video t)))
              (27 (setq attrs (plist-put attrs :inverse-video nil)))
              ;; half-bright, blink, and mapping/font stuff not supported
              (38
               (cond
                ((and (equal (car parameters) 5) (setq p (cadr parameters)))
                 (setq parameters (cddr parameters))
                 ;; If out of range, nil is fine?
                 (setq attrs (plist-put attrs :foreground (impru-sgr-get-color p))))
                (t (setq attrs
                         (plist-put (plist-put attrs :underline t)
                                    :foreground nil)))))
              (39 (setq attrs (plist-put (plist-put attrs :underline nil)
                                         :foreground nil)))
              (48
               (when (and (equal (car parameters) 5) (setq p (cadr parameters)))
                 (setq parameters (cddr parameters))
                 ;; If out of range, nil is fine?
                 (setq attrs (plist-put attrs :background (impru-sgr-get-color p)))))
              (49 (setq attrs (plist-put attrs :background nil)))
              (otherwise
               (cond
                ((<= 30 p 37)
                 (setq attrs (plist-put attrs :foreground
                                        (impru-sgr-get-color (- p 30)))))
                ((<= 40 p 47)
                 (setq attrs (plist-put attrs :background
                                        (impru-sgr-get-color (- p 40)))))))))
          (funcall forward (append '(vt sgr-face) attrs))
          (setf (impru-sgr-state-attrs state) attrs))))))

(defun impru-sgr--decode (state octets start end reply forward)
  (if (not (stringp octets))
      (funcall forward octets start end)
    (let ((pos start))
      (while (< pos end)
        (cond
         ((impru-sgr-state-after-esc state)
          ;; Always advance by at least one octet. We always append to this octet by octet, so if
          ;; we had a terminating sequence we should've already handled it earlier.
          (let ((after-esc (concat (impru-sgr-state-after-esc state)
                                   (string (aref octets pos)))))
            (setq pos (1+ pos))
            (cl-case (aref after-esc 0)
              (?\[
               ;; CSI. Look for the terminator.
               (let* ((buf-padding-amount (max 0 (- impru-sgr--max-csi-length (length after-esc))))
                      (esc-buf (concat after-esc (make-string buf-padding-amount 0 nil)))
                      (esc-pos (length after-esc))
                      csi-done-p)
                 (while (and (< pos end) (< esc-pos (length esc-buf))
                             (not (setq csi-done-p (impru-sgr--csi-done-p esc-buf 1 esc-pos))))
                   (aset esc-buf esc-pos (aref octets pos))
                   (setq pos (1+ pos) esc-pos (1+ esc-pos)))
                 (if (not csi-done-p)
                     (setf (impru-sgr-state-after-esc state)
                           (substring esc-buf 0 esc-pos))
                   (impru-sgr--process-csi state esc-buf 1 esc-pos forward)
                   (setf (impru-sgr-state-after-esc state) nil))))
              ((?% ?\( ?\))
               ;; Ignore charset stuff.
               (when (= 2 (length after-esc))
                 (setf (impru-sgr-state-after-esc state) nil)))
              (otherwise
               ;; Ignore other stuff.
               (setf (impru-sgr-state-after-esc state) nil)))))
         (t
          ;; Not after ESC.
          (let ((pass-start pos) (pass-end pos) (after-esc nil))
            (while (and (null after-esc) (< pos end))
              (cl-case (aref octets pos)
                (#x07                             ; BEL
                 (funcall forward '(vt bell)))
                ((#x18 #x1a)                      ; CAN, SUB
                 nil)                             ; ignore
                (#x1b                             ; ESC
                 (setf after-esc ""))
                (#x9b                             ; CSI
                 (setf after-esc "["))
                (otherwise
                 (when (= pos pass-end)
                   (setq pass-end (1+ pass-end)))))
              (setq pos (1+ pos))
              (unless (= pos pass-end)
                (when (< pass-start pass-end)
                  (funcall forward octets pass-start pass-end))
                (setq pass-start pos pass-end pos)))
            (setf (impru-sgr-state-after-esc state) after-esc)
            (when (< pass-start pass-end)
              (funcall forward octets pass-start pass-end)))))))))

(defun impru-sgr--encode (_state p1 p2 p3 _reply forward)
  (funcall forward p1 p2 p3))

(impru-protocol-define-layer-class
 'sgr
 #'make-impru-sgr-state
 #'impru-sgr--encode
 #'impru-sgr--decode)

(provide 'impru-sgr)

;;; impru-sgr.el ends here
