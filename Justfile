# -*- makefile -*- (not really, but close enough for editing)
# SPDX-License-Identifier: CC-PDDC
# 
# This file defines tasks to be conveniently run, without the dependency and
# build properties of a Makefile. It can be read by the ‘just’ program, whose
# website (as of 2022-01-22) is at https://github.com/casey/just.
# 
# In this case, we use it to wrap Nattōmaki, which lives in maint/ and is
# run using emacs --batch.

distel := "emacs --batch -Q -l ./maint/nattomaki.el --eval '(kill-emacs (nttmk-cli-run))' --"
build-directory := "built"

default:
	@just --list

build:
	@{{distel}} build-directory={{build-directory}} build

clean:
	@{{distel}} build-directory={{build-directory}} clean

package:
	@{{distel}} build-directory={{build-directory}} package

# Local variables:
# fill-column: 76
# End:
