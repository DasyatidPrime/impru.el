;;; impru-protocol.el --- Protocol stacking for Imprū                    -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-08
;; Version: 0.2.0
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:



;;; Code:

(require 'cl-lib)

(defvar impru-protocol-stack nil)

(cl-defstruct (impru-protocol-stack
               (:constructor make-impru-protocol-stack
                             (send-function received-function)))
  ;; zeroth is "top"/outermost
  (layers (vector))
  send-function
  received-function)

(cl-defstruct (impru-protocol-layer-class
               (:constructor make-impru-protocol-layer-class
                             (name
                              make-state-function
                              encode-function
                              decode-function)))
  name
  make-state-function
  encode-function
  decode-function)

(cl-defstruct (impru-protocol-layer
               (:constructor make-impru-protocol-layer (class state)))
  class
  state)

(defvar impru-protocol-layer-class-table (make-hash-table :test #'eq :size 4))

(defun impru-protocol-start-layer (class args)
  (let* ((class& (gethash class impru-protocol-layer-class-table))
         (state (apply (impru-protocol-layer-class-make-state-function class&)
                       args)))
    (make-impru-protocol-layer class& state)))

(defun impru-protocol-stack-push (stack class &rest args)
  (let ((layer (impru-protocol-start-layer class args)))
    (setf (impru-protocol-stack-layers stack)
          (vconcat (impru-protocol-stack-layers stack)
                   (vector layer)))
    (impru-protocol-layer-state layer)))

(defun impru-protocol-stack--decode-from (stack p1 p2 p3 layer-index)
  (if (>= layer-index (length (impru-protocol-stack-layers stack)))
      ;; decode to local
      (funcall (impru-protocol-stack-received-function stack) p1 p2 p3)
    (let* ((layer (aref (impru-protocol-stack-layers stack) layer-index))
           (class& (impru-protocol-layer-class layer))
           (state (impru-protocol-layer-state layer))
           (forward
            (lambda (p1 &optional p2 p3)
              (impru-protocol-stack--decode-from
               stack p1 (or p2 0) (or p3 (length p1)) (1+ layer-index))))
           (reply
            (lambda (p1 &optional p2 p3)
              (impru-protocol-stack--encode-from
               stack p1 (or p2 0) (or p3 (length p1)) (1- layer-index)))))
      (funcall (impru-protocol-layer-class-decode-function class&)
               state p1 p2 p3 reply forward))))

(defun impru-protocol-stack-decode (stack data)
  (impru-protocol-stack--decode-from stack data 0 (length data) 0))

(defun impru-protocol-stack--encode-from (stack p1 p2 p3 layer-index)
  (if (< layer-index 0)
      ;; encode to remote
      (funcall (impru-protocol-stack-send-function stack) p1 p2 p3)
    (let* ((layer (aref (impru-protocol-stack-layers stack) layer-index))
           (class& (impru-protocol-layer-class layer))
           (state (impru-protocol-layer-state layer))
           (forward
            (lambda (p1 &optional p2 p3)
              (impru-protocol-stack--encode-from
               stack p1 (or p2 0) (or p3 (length p1)) (1- layer-index))))
           (reply
            (lambda (p1 &optional p2 p3)
              (impru-protocol-stack--decode-from
               stack p1 (or p2 0) (or p3 (length p1)) (1+ layer-index)))))
      (funcall (impru-protocol-layer-class-encode-function class&)
               state p1 p2 p3 reply forward))))

(defun impru-protocol-stack-encode (stack data)
  (impru-protocol-stack--encode-from
   stack data 0 (length data)
   (1- (length (impru-protocol-stack-layers stack)))))

(defun impru-protocol-define-layer-class (name make-state encode decode)
  (puthash name
           (make-impru-protocol-layer-class
            name make-state encode decode)
           impru-protocol-layer-class-table))

(defun impru-coding-system--make-state (coding-system)
  (record 'impru-coding-system coding-system))

(defun impru-coding-system--encode (state chars start end reply forward)
  (if (not (stringp chars))
      (funcall forward chars start end)
    (let* ((subchars (if (and (= start 0) (= end (length chars))) chars
                       (substring chars start end)))
           (octets (encode-coding-string subchars (aref state 1) t)))
      (funcall forward octets 0 (length octets)))))

(defun impru-coding-system--decode (state octets start end reply forward)
  (if (not (stringp octets))
      (funcall forward octets start end)
    (let* ((suboctets (if (and (= start 0) (= end (length octets))) octets
                        (substring octets start end)))
           (chars (decode-coding-string suboctets (aref state 1) t)))
      (funcall forward chars 0 (length chars)))))

(impru-protocol-define-layer-class
 'coding-system
 #'impru-coding-system--make-state
 #'impru-coding-system--encode
 #'impru-coding-system--decode
 )

(provide 'impru-protocol)

;;; impru-protocol.el ends here
