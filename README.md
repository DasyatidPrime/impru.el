<!-- SPDX-License-Identifier: GPL-3.0-only -->

# Imprū

Imprū is an Emacs-based client for certain networked text-based virtual
worlds, sometimes known as MU*. It is primarily targeted at interacting with
the line-oriented MUCK/MOO/MUSH families of servers and is less likely to be
suited to MUDs of the Aber, Diku, or LP style that make heavier use of
terminal features.

Right now, it's mostly a pile of code. You can actually get a connection
through with it though! I think!

Unless you're really interested in hacking on this, better wait until
there's actually some kind of announcement, perhaps.

## Name origin

The name can be considered an ellipsis of “impromptu”, referring either to
the category of musical composition (thus a riff on [TinyFugue][], another
piece of software with a similar purpose) or (by complete etymological
perversion) to the lack of incomplete-line prompts from the world servers
this is primarily targeted at.

It can alternatively be read as an initialism: Interface to Multi-user
Prose-based Realities and Unrealities.

[TinyFugue]: http://tinyfugue.sourceforge.net/
