;;; impru-edit.el --- Interactive configuration for Imprū                -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-10
;; Version: 0.2.0
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:



;;; Code:

(require 'seq)
(require 'wid-edit)
(require 'impru)

(defface impru-world-edit-preamble-face
  '((t))
  "Face for the opening line of a world editor buffer."
  :group 'impru-faces)

(defface impru-world-edit-heading-face
  '((t :inherit custom-variable-tag))
  "Face for heading/label text in the world editor.
This is used fairly frequently to introduce widgets."
  :group 'impru-faces)

(defface impru-world-edit-static-value-face
  '((t :inherit bold))
  "Face for fixed values in the world editor.
This is used infrequently."
  :group 'impru-faces)

(defvar-local impru-world-edit-world-name nil)
(defvar-local impru-world-edit-worldspec nil)

(defun impru-world-edit--buffer-name (world-name)
  (format "*Imprū World Definition: %s*" world-name))

(defun impru-world-edit--get-type-for-widget ()
  (plist-get impru-world-edit-worldspec :type))

(defun impru-world-edit--update-type-from-widget (widget &rest _)
  (setq impru-world-edit-worldspec
        (plist-put impru-world-edit-worldspec :type (widget-value widget))))

(defun impru-world-edit--present-hostspec-widget (hostspec)
  (list (or (plist-get hostspec :host) "")
        (if-let ((port (plist-get hostspec :port)))
            (impru-present-port port)
          "")
        (and (plist-get hostspec :tls) t)))

(defun impru-world-edit--interpret-hostspec-widget (wvalue)
  (pcase-exhaustive wvalue
    (`(,host ,port ,tls)
     (list :host (string-trim host)
           :port (impru-interpret-port (string-trim port))
           :tls tls))))

(defun impru-world-edit--validate-port-widget (widget)
  (unless (impru-valid-port-string-p (widget-value widget))
    (prog1 widget
      (widget-put widget :error (get 'impru-invalid-port-string 'error-message)))))

(defun impru-world-edit--get-hostspecs-for-widget ()
  (or
   (mapcar #'impru-world-edit--present-hostspec-widget
           (plist-get impru-world-edit-worldspec :hostspecs))
   (list (list "" "" nil))))

(defun impru-world-edit--update-hostspecs-from-widget (widget &rest _)
  (catch 'no-update
    (let ((new-hostspecs (condition-case nil
                             (mapcar #'impru-world-edit--interpret-hostspec-widget
                                     (widget-value widget))
                           (impru-invalid-port-string (throw 'no-update nil)))))
      (setq impru-world-edit-worldspec
            (plist-put impru-world-edit-worldspec :hostspecs new-hostspecs)))))

(defun impru-world-edit--get-logins-for-widget ()
  (mapcar #'car (plist-get impru-world-edit-worldspec :logins)))

(defun impru-world-edit--update-logins-from-widget (widget &rest _)
  ;; XXX: temporarily disabled while reworking login options format
  ;; (setq impru-world-edit-worldspec
  ;;       (plist-put impru-world-edit-worldspec :logins
  ;;                  (mapcar #'string-trim (widget-value widget))))
  ;;
  ;; Note that you can't just try to match up the existing options naively, because this gets
  ;; called too often. If the user inserts or deletes a single character of the name, then you have
  ;; no idea what was intended with the rest of the options. So for now we preserve existing data
  ;; as much as possible.
  )

(declare-function impru-world-edit--refresh "impru-edit")

(defun impru-world-edit-reload (&optional show-messages)
  (interactive (list t))
  (setq impru-world-edit-worldspec (impru--get-user-world impru-world-edit-world-name))
  (impru-world-edit--refresh)
  (goto-char (point-min))
  (recenter 1)
  (when show-messages
    (message "Reloaded current world definition")))

(defun impru-world-edit--reload-button (&rest _)
  (call-interactively #'impru-world-edit-reload))

(defun impru-world-edit-revert (&optional show-messages)
  ;; TODO: duplication with above
  (interactive (list t))
  (impru-revert-user-world impru-world-edit-world-name)
  (setq impru-world-edit-worldspec (impru--get-user-world impru-world-edit-world-name))
  (impru-world-edit--refresh)
  (goto-char (point-min))
  (recenter 1)
  (when show-messages
    (message "Reverted to saved world definition")))

(defun impru-world-edit--revert-button (&rest _)
  (call-interactively #'impru-world-edit-revert))

(defun impru-world-edit-reload-or-revert (prefix)
  (interactive "P")
  (if prefix
      (call-interactively #'impru-world-edit-revert)
    (call-interactively #'impru-world-edit-reload)))

(defun impru-world-edit-apply (&optional show-messages)
  (interactive (list t))
  (impru--replace-user-world impru-world-edit-world-name
                             (copy-sequence impru-world-edit-worldspec))
  (when show-messages
    (message "Updated world definition for this session")))

(defun impru-world-edit--apply-button (&rest _)
  (call-interactively #'impru-world-edit-apply))

(defun impru-world-edit-apply-save (&optional show-messages)
  (interactive (list t))
  (impru--replace-user-world impru-world-edit-world-name
                             (copy-sequence impru-world-edit-worldspec))
  (impru-save-user-world impru-world-edit-world-name)
  (when show-messages
    (message "Saved world definition")))

(defun impru-world-edit--apply-save-button (&rest _)
  (call-interactively #'impru-world-edit-apply-save))

(defun impru-world-edit-delete-world ()
  (interactive)
  (when (yes-or-no-p (format "Delete world definition for %s? " impru-world-edit-world-name))
    ;; TODO: clear creds and such?
    (impru--delete-user-world impru-world-edit-world-name)
    (kill-buffer (current-buffer))))

(defun impru-world-edit--delete-world-button (&rest _)
  (call-interactively #'impru-world-edit-delete-world))

(defvar-local impru-world-edit--widgets '())

(defun impru-world-edit--set-widgets-loosely (&rest pile)
  (setq impru-world-edit--widgets
        (seq-uniq (seq-filter #'widgetp pile))))

(defun impru-world-edit--refresh ()
  ;; TODO: unsnark this
  (dolist (irritating impru-world-edit--widgets)
    (widget-delete irritating))
  (let ((inhibit-read-only t)
        (inhibit-modification-hooks t))
    (erase-buffer))
  (impru-world-edit--set-widgets-loosely
   (widget-insert (propertize "Editing world definition:"
                              'face 'impru-world-edit-preamble-face)
                  "\n")
   (widget-insert "\n" (propertize "Name:" 'face 'impru-world-edit-heading-face)
                  " " (propertize (format "%s" impru-world-edit-world-name)
                                  'face 'impru-world-edit-static-value-face)
                  " ")
   (apply #'widget-create 'menu-choice
          :format " Type: %[[Choose]%] %v "
          :value (impru-world-edit--get-type-for-widget)
          :notify #'impru-world-edit--update-type-from-widget
          (mapcar (lambda (entry)
                    `(item :format "%t"
                           :tag ,(or (plist-get (cdr entry) :label) "(no label)")
                           ,(car entry)))
                  impru-world-type-alist))
   (widget-insert "\n\n" (propertize "Connect to:" 'face 'impru-world-edit-heading-face) "\n")
   ;; TODO: can we auto-trim whitespace on these?
   (widget-create 'editable-list
                  :entry-format "%i %d %v\n"
                  :indent 2
                  :value (impru-world-edit--get-hostspecs-for-widget)
                  :notify #'impru-world-edit--update-hostspecs-from-widget
                  '(group
                    :format "%v"
                    (editable-field :size 31 :format "Host: %v ")
                    (editable-field :size 5 :format " Port: %v "
                                    :validate #'impru-world-edit--validate-port-widget)
                    (checkbox :format " %[%v Use TLS%] ")
                    ))
   (widget-insert "\n" (propertize "Login as:" 'face 'impru-world-edit-heading-face) "\n")
   (widget-create 'editable-list
                  :entry-format "%i %d %v\n"
                  :indent 2
                  :value (impru-world-edit--get-logins-for-widget)
                  :notify #'impru-world-edit--update-logins-from-widget
                  '(editable-field :size 15))
   (widget-insert "\n")
   (widget-create 'push-button :notify #'impru-world-edit--reload-button
                  "Reload")
   (widget-insert " ")
   (widget-create 'push-button :notify #'impru-world-edit--revert-button
                  "Revert")
   (widget-insert " ")
   (widget-create 'push-button :notify #'impru-world-edit--apply-button
                  "Apply")
   (widget-insert " ")
   (widget-create 'push-button :notify #'impru-world-edit--apply-save-button
                  "Apply and Save")
   (widget-insert "\n\n")
   (widget-create 'push-button :notify #'impru-world-edit--delete-world-button
                  "Delete World")
   (widget-insert "\n"))
  (widget-setup)
  (set-buffer-modified-p nil))

(define-derived-mode impru-world-edit-mode nil "Imprū Edit"
  "TODO: docstring")

(let ((map impru-world-edit-mode-map))
  (set-keymap-parent impru-world-edit-mode-map widget-keymap)
  (define-key map [?\C-c ?\C-r] #'impru-world-edit-reload-or-revert)
  (define-key map [?\C-c ?\C-c] #'impru-world-edit-apply)
  (define-key map [?\C-x ?\C-s] #'impru-world-edit-apply-save)
  (define-key map [?\C-c delete] #'impru-world-edit-delete-world))

;;;###autoload
(defun impru-edit-world (world-name)
  (interactive (list (impru-read-world-name t)))
  (setq world-name (impru--normalize-world-name world-name))
  (let ((worldspec (impru--get-user-world world-name))
        (buffer (get-buffer-create (impru-world-edit--buffer-name world-name))))
    (switch-to-buffer buffer)
    (with-current-buffer buffer
      (unless (eq world-name impru-world-edit-world-name)
        (impru-world-edit-mode)
        (setq impru-world-edit-world-name world-name)
        (setq impru-world-edit-worldspec
              (or worldspec (list :name world-name)))
        (impru-world-edit--refresh)
        (goto-char (point-min))
        (recenter 1)))))

;;;###autoload (autoload 'mnrcr-edit-world "impru-edit")
(impru-defcompat mnrcr 'mnrcr-edit-world 'impru-edit-world)

(defconst impru-world-list--tabulated-list-format
  '[("-*" 2 nil)
    ("World" 15 t)
    ("Type" 7 nil)
    ("Login" 15 t)
    ("Port" 7 nil)
    ("Hostname" 27 t)])

(defvar impru-world-list--alist-variable 'impru-user-world-alist)
(defvar-local impru-world-list--transient-info-alist nil)

(defsubst impru-world-list--borrow-world-alist ()
  (symbol-value impru-world-list--alist-variable))

(defsubst impru-world-list--copy-world-alist ()
  (copy-alist (symbol-value impru-world-list--alist-variable)))

(defsubst impru-world-list--replace-world-alist (new-alist)
  (set impru-world-list--alist-variable new-alist))

(defsubst impru-world-list--update-world (name world-plist)
  (setcdr (assq name (impru-world-list--borrow-world-alist)) world-plist))

(defun impru-world-list--present-hostspec (hostspec)
  (vector
   (concat (if-let ((port (plist-get hostspec :port)))
               (int-to-string port) "")
           (if (plist-get hostspec :tls) "@" ""))
   (or (plist-get hostspec :host) "")))

(defun impru-world-list--fetch-entries ()
  (let ((rows '()))
    (dolist (world-entry (sort (impru-world-list--copy-world-alist) #'impru--symbol-key-<))
      (let (further-logins further-hostspecs)
        (let* ((name (car world-entry)) (worldspec (cdr world-entry))
               (row-id name)
               (mark-char (or (plist-get (cdr (assq name impru-world-list--transient-info-alist))
                                         :mark)
                              ?\ ))
               (mark-column (string ?\  mark-char))
               (name (format "%s" name))
               (type
                (let ((raw-type (plist-get worldspec :type)))
                  (if raw-type
                      (if-let ((known (cdr (assq-type raw impru-world-type-alist)))
                               (label (plist-get known :label)))
                          label
                        (format "%s" raw-type))
                    ""))) ; TODO: propertize
              (login (if-let ((logins (plist-get worldspec :logins)))
                         (concat (car logins) (and (cdr logins) "+"))
                       "")) ; TODO: better display (and below)
              host+
              (host (if-let ((hostspecs (plist-get worldspec :hostspecs))
                             (host (plist-get (car hostspecs) :host)))
                        (concat (plist-get (car hostspecs) :host)
                                (and (cdr hostspecs)
                                     (not (seq-every (lambda (other)
                                                       (equal host (plist-get other :host)))
                                                     (cdr hostspecs)))
                                     (setq host+ "+")))
                      ""))
              (port (if-let ((hostspecs (plist-get worldspec :hostspecs))
                             (port (plist-get (car hostspecs) :port)))
                        (concat (impru-present-port port)
                                (and (cdr hostspecs) (null host+) "+"))
                      "")))
          (push (list row-id (vector mark-column name type login port host)) rows))))
    (nreverse rows)))

(define-derived-mode impru-world-list-mode tabulated-list-mode "Imprū World List"
  "Major mode for editing available Imprū worlds."
  (setq-local tabulated-list-format impru-world-list--tabulated-list-format
              tabulated-list-entries #'impru-world-list--fetch-entries
              impru-world-list--transient-info-table (make-hash-table :test #'eq :size 1))
  (tabulated-list-init-header))

(defconst impru-world-list-buffer-name "*Imprū World List*")

;;;###autoload
(defun impru-list-worlds ()
  (interactive)
  (let ((buffer (get-buffer-create impru-world-list-buffer-name)))
    (with-current-buffer buffer
      (impru-world-list-mode)
      (tabulated-list-print))
    (pop-to-buffer buffer)))

;;;###autoload (autoload 'mnrcr-list-worlds "impru-edit")
(impru-defcompat mnrcr 'mnrcr-list-worlds 'impru-list-worlds)

(defun impru-world-list--call-with-world-at-point (thunk &optional noerror)
  (if-let ((name (tabulated-list-get-id))
           (entry (assq name (impru-world-list--borrow-world-alist))))
      (funcall thunk (car entry) (cdr entry))
    (unless noerror
      (error "No world at point"))))

(defmacro impru-world-list--with-world-at-point (vars args &rest body)
  (declare (indent 2))
  `(impru-world-list--call-with-world-at-point
    (lambda (,(nth 0 vars) ,(nth 1 vars)) ,@body)
    ,@args))

(defun impru-world-list-edit-world ()
  (interactive)
  (unless (eq 'impru-user-world-alist impru-world-list--alist-variable)
    (error "Don't know how to edit worlds in this list"))
  (impru-world-list--with-world-at-point (world-name _) (t)
    (impru-edit-world world-name)))

(defun impru-world-list-connect (&optional prefix)
  (interactive "P")
  (impru-world-list--with-world-at-point (world-name _) ()
    (impru-connect-to-world
     world-name (impru-read-login-name--prefix-arg world-name prefix))))

(let ((map impru-world-list-mode-map))
  ;; TODO: what should C-x C-s do here? In particular, how do we handle
  ;; saving deletions?
  (define-key map [?\C-m] 'impru-world-list-edit-world)
  (define-key map [return] 'impru-world-list-edit-world)
  (define-key map [?\C-c ?\C-o] 'impru-world-list-connect))

(provide 'impru-edit)

;;; impru-edit.el ends here
