;;; impru-telnet.el --- Telnet support for Imprū                         -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-08
;; Version: 0.2.0
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:



;;; Code:

(require 'cl-lib)
(require 'impru-protocol)

(cl-defstruct (impru-telnet-state
               (:constructor make-impru-telnet-state ()))
  (held-command nil) ; either (STR SCAN-MODE . SCAN-PARAMS) or nil
  (active-options (vector '() '())) ; incoming outgoing
  )

(defun impru-telnet--protocol-warning (state format-string &rest args)
  (apply #'warn (concat "Telnet: " format-string) args))

(defun impru-telnet--unescape (octets start end &optional escaped-count)
  (let ((octets* (make-string (- (length octets) (or escaped-count 0)) 0 nil))
        (pos start) (pos* 0))
    (while (< pos end)
      (let ((octet (aref octets pos)))
        (aset octets* pos* octet)
        (setq pos (if (= octet 255) (+ 2 pos) (1+ pos))
              pos* (1+ pos*))))
    (if (= pos* (length octets*)) octets*
      (substring octets* 0 pos*))))

(defun impru-telnet--scan-command (octets iac1 end)
  ;; iac1 is one octet after initial IAC.
  ;; Return: (scan-more LIKELY-LEN) | (scan-for OCTET) | (scanned END-POS COMMAND ...)
  ;; LIKELY-LEN is advisory; min is always 1.
  (if (= iac1 end) '(scan-more 2)
    (let ((selector (aref octets iac1)))
      (cond
       ((<= 251 selector 254)
        ;; Base negotiation.
        (if (< (- end iac1) 2) '(scan-more 1)
          (list 'scanned (+ 2 iac1)
                (aref '[will wont do dont]
                      (- (aref octets iac1) 251))
                (aref octets (1+ iac1)))))
       ((<= 240 selector 249)
        ;; One-shot functions.
        (list 'scanned (1+ iac1)
              (aref '[se nop mark brk ip ao ayt ec el ga]
                    (- selector 240))))
       ((= selector 255)
        (error "IAC IAC should have been processed elsewhere"))
       ((= selector 250)
        ;; Subnegotiation. This is the complicated one.
        (if (< (- end iac1) 3) '(scan-more 7)     ; Rough approximation.
          (catch 'done
            ;; TODO: what if opt is 255?
            (let ((opt (aref octets (1+ iac1))) (probe (+ 2 iac1))
                  (sub-end nil) (escaped-count 0) (any-garbage nil))
              (while (and (null sub-end) (< probe end))
                (cond
                 ((/= 255 (aref octets probe))
                  (setq probe (1+ probe)))
                 ((< (- end probe) 2)
                  (throw 'done '(scan-for 240)))
                 ((= 255 (aref octets (1+ probe)))
                  (setq escaped-count (1+ escaped-count)
                        probe (+ 2 probe)))
                 ((= 240 (aref octets (1+ probe)))
                  (setq sub-end probe))
                 (t
                  ;; IAC something-unexpected.
                  (setq any-garbage t
                        sub-end probe))))
              (cond
               (any-garbage (list 'scanned sub-end 'burnt-edge))
               ((null sub-end) '(scan-for 240))
               (t
                (list 'scanned (+ 2 sub-end) 'subnegotiation opt
                      (if (zerop escaped-count)
                          (substring octets (+ 2 iac1) sub-end)
                        (impru-telnet--unescape octets (+ 2 iac1) sub-end
                                                escaped-count)))))))))
       (t
        ;; Unknown selector.
        (list 'scanned (1+ iac1) 'unknown selector))))))

(defvar impru-telnet--option-handler-alist
  '())

(defun impru-telnet--handle-option (state number incomingp enablep reply forward)
  (if-let ((option-handler (cdr (assoc number impru-telnet--option-handler-alist))))
      (let* ((optionses (impru-telnet-state-active-options state))
             (slot (if incomingp 0 1))
             (pointer (or (assoc number (aref optionses slot))
                          (car (push (cons number nil) (aref optionses slot))))))
        (when (xor (and (cdr pointer) t) enablep)
          (funcall option-handler state pointer incomingp enablep reply)))
    ;; Unrecognized options are never enabled, so always refuse them.
    (when enablep
      (funcall reply (unibyte-string 255 (if incomingp 254 252) number) 0 3))))

(defun impru-telnet--handle-command (state command parameters reply forward)
  (cl-case command
    ((will wont do dont)
     (impru-telnet--handle-option state (car parameters)
                                  (memq command '(will wont)) (memq command '(will do))
                                  reply forward))
    ((subnegotiation)
     (impru-telnet--handle-subnegotiation state (car parameters) (cadr parameters) reply))
    (ga (funcall forward '(telnet go-ahead)))
    (se (impru-telnet--protocol-warning
         state "Unexpected SE (no subnegotiation active)"))
    (nop nil)
    (mark (impru-telnet--protocol-warning
           state "Telnet synch is not handled here"))
    ((brk ip ao ayt ec el)
     (impru-telnet--protocol-warning
      state "%s should probably not come from the other side" (upcase (symbol-name command))))
    (otherwise
     (impru-telnet--protocol-warning
      state "Sent myself command %s, but what is that?" (upcase (symbol-name command))))))

(defun impru-telnet--decode (state octets start end reply forward)
  (if (not (stringp octets))
      (funcall forward octets start end)
    (let ((pos start))
      (while (< pos end)
        (pcase-exhaustive (impru-telnet-state-held-command state)
          (`(,start-string ,scan-mode . ,scan-parameters)
           (let* ((maybe-command-end
                   (cl-ecase scan-mode
                     (scan-more (min end (+ pos (car scan-parameters))))
                     (scan-for
                      (let ((probe pos) (needle (car scan-parameters)))
                        ;; TODO: surely there's a function for this?
                        (while (and (< probe end) (/= (aref octets probe) needle))
                          (setq probe (1+ probe)))
                        (min end (1+ probe))))))
                  (scannable (concat start-string (substring octets pos maybe-command-end))))
             (pcase-exhaustive (impru-telnet--scan-command scannable 0 (length scannable))
               ((and continuation `(,(or 'scan-more 'scan-for) . ,_))
                (setf (impru-telnet-state-held-command state)
                      (cons scannable continuation))
                (setq pos maybe-command-end))
               (`(scanned ,command-end-within-scannable ,command . ,parameters)
                (setf (impru-telnet-state-held-command state) nil)
                (impru-telnet--handle-command state command parameters reply forward)
                ;; The separate scannable starts at (- pos (length start-string)),
                (let ((scannable-starts-at (- pos (length start-string))))
                  (setq pos (+ scannable-starts-at command-end-within-scannable))
                  ;; It's possible for command-end-within-scannable to have been within the earlier
                  ;; string if the scanner backtracked to an error. Just recurse in that case; it's
                  ;; not worth giving the outer loop an explicit stack to work with.
                  (when (< pos 0)
                    (impru-telnet--decode
                     state scannable command-end-within-scannable (length scannable)
                     reply forward)
                    (setq pos 0)))))))
          ('nil
           (let ((data-end pos) (escaped-count 0) (found-command nil))
             (while (and (not found-command) (< data-end end))
               (cond
                ((/= 255 (aref octets data-end))
                 (setq data-end (1+ data-end)))
                ((or (= (1+ data-end) end) (/= 255 (aref octets (1+ data-end))))
                 (setq found-command t))
                (t
                 (setq escaped-count (1+ escaped-count)
                       data-end (+ 2 data-end)))))
             (when (< pos data-end)
               (if (zerop escaped-count)
                   (funcall forward octets pos data-end)
                 (funcall forward (impru-telnet--unescape octets pos data-end escaped-count)
                          0 (- data-end pos escaped-count))))
             (setq pos data-end)
             (when found-command
               ;; IAC is at data-end.
               (pcase-exhaustive (impru-telnet--scan-command octets (1+ data-end) end)
                 ((and continuation `(,(or 'scan-more 'scan-for) . ,_))
                  (setf (impru-telnet-state-held-command state)
                        (cons (substring octets (1+ data-end) end) continuation))
                  (setq pos end))
                 (`(scanned ,command-end ,command . ,parameters)
                  (impru-telnet--handle-command state command parameters reply forward)
                  (setq pos command-end)))))))))))

(defun impru-telnet--encode (state octets start end reply forward)
  (if (not (stringp octets))
      (funcall forward octets start end)
    (let ((need-escape-count 0))
      (let ((pos start))
        (while (< pos end)
          (when (= 255 (aref octets pos))
            (setq need-escape-count (1+ need-escape-count)))
          (setq pos (1+ pos))))
      (if (zerop need-escape-count)
          (funcall forward octets start end)
        ;; TODO: limit buffer size?
        (let ((escaped (make-string (+ (- end start) need-escape-count) 0 nil))
              (pos start) (pos* 0))
          (while (< pos end)
            (let ((octet (aref octets pos)))
              (aset escaped pos* octet)
              (if (/= octet 255)
                  (setq pos (1+ pos) pos* (1+ pos*))
                (aset escaped (1+ pos*) 255)
                (setq pos (1+ pos) pos* (+ 2 pos*)))))
          (cl-assert (= pos* (length escaped)))
          (funcall forward escaped 0 pos*))))))

(impru-protocol-define-layer-class
 'telnet
 #'make-impru-telnet-state
 #'impru-telnet--encode
 #'impru-telnet--decode)

(provide 'impru-telnet)

;;; impru-telnet.el ends here
