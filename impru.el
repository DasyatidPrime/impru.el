;;; impru.el --- MU* text world client                                   -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-07
;; Version: 0.2.0
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:

;; Imprū is an Emacs-based client for certain networked text-based
;; virtual worlds, sometimes known as MU*. It is primarily targeted at
;; interacting with the line-oriented MUCK/MOO/MUSH families of servers and
;; is less likely to be suited to MUDs of the Aber, Diku, or LP style that
;; make heavier use of terminal features.

;; See the included Info manual for more information.

;;; Code:

;;(require 'auth-source)
(require 'cl-lib)
(require 'gnutls)
(require 'macroexp)
(require 'impru-protocol)
(require 'impru-telnet)
(require 'impru-sgr)
(require 'impru-mcp)

(eval-and-compile
  (defconst impru-version "0.2.0")
  (defconst impru-version-list (eval-when-compile (version-to-list impru-version))))

(defgroup impru nil
  "The Imprū client for text-based virtual worlds (MU*)."
  :group 'applications
  :prefix "impru-")

(defgroup impru-faces nil
  "Faces for display in Imprū."
  :group 'impru
  :prefix "impru-")

(defcustom impru-use-auth-source t
  "Try to read credentials from ‘auth-source’.
This allows profile-wide credential storage to be used for auto-
login. Note that saving new credentials this way is not supported.
You can customize the variable ‘auth-sources’ to determine where
to search.

This is the same library used by Gnus, if you are familiar with
that. See the Info node ‘(auth)Help for users’ for more details."
  :group 'impru
  :type 'boolean)

(defun impru-use-auth-source (&optional load-if-needed)
  (and impru-use-auth-source
       (if load-if-needed
           (require 'auth-source nil t)
         (featurep 'auth-source))))

(eval-and-compile
  (defconst impru-compatibility-switches '(mnrcr)
    "Load-or-compile-time list of compatibility switches to enable.
Each element is a symbol. The following symbols are meaningful:

  mnrcr -- Compatibility with previous name with ‘mnrcr-’ prefix."))

(defmacro impru-ifcompat (compat-switch if-true &optional if-false)
  "Choose an expression depending on a compatibility switch.
COMPAT-SWITCH is a symbol for ‘impru-compatibility-switches’.
If the switch is enabled, this macro expands to IF-TRUE;
otherwise, it expands to IF-FALSE."
  (declare (indent 2))
  (if (memq compat-switch impru-compatibility-switches)
      if-true
    if-false))

(defmacro impru-whencompat (compat-switch &rest forms)
  "Include or exclude expressions depending on a compatibility switch.
COMPAT-SWITCH is a symbol for ‘impru-compatibility-switches’.
If the switch is enabled, this macro is equivalent to ‘progn’
across FORMS; otherwise, it expands to nil."
  (declare (indent 1))
  (if (memq compat-switch impru-compatibility-switches)
      (macroexp-progn forms)
    nil))

(defmacro impru-defcompat (compat-switch alias target)
  "Like ‘defalias’, but only if a compatibility switch is enabled.
COMPAT-SWITCH is a symbol for ‘impru-compatibility-switches’.
The alias is only defined if COMPAT-SWITCH is enabled. As with
‘defalias’, ALIAS and TARGET are not automatically quoted.

Note that plain autoload cookies won't work as expected; see
the Info node ‘(impru)Internals warts’. As a workaround, you
can manually specify an unconditional autoload form."
  (declare (indent 1))
  (when (memq compat-switch impru-compatibility-switches)
    `(defalias ,alias ,target)))


;;;; Private utilities

(defun impru--symbol< (a b)
  "Return non-nil if A precedes B by symbol name.
A and B must be symbols. This is useful as a ‘sort’ predicate."
  (string< (symbol-name a) (symbol-name b)))

(defun impru--symbol-key-< (a b)
  "Return non-nil if A precedes B by symbol name of car.
A and B must be conses whose cars are symbols. This is useful as
a ‘sort’ predicate."
  (string< (symbol-name (car a)) (symbol-name (car b))))

(defun impru--plist-delete-1 (plist key)
  "Delete one instance of KEY from PLIST, destructively.
Returns the new plist."
  (let* ((head (cons nil plist)) (probe head))
    (while probe
      (if (eq key (cadr probe))
          (setf (cdr probe) (cdddr probe) 
                probe nil)
        (setq probe (cddr probe))))
    (cdr head)))

(defun impru--plist-delete-all (plist &rest keys)
  "Delete all instances of KEYS from PLIST, destructively.
Returns the new plist."
  (let* ((head (cons nil plist)) (probe head))
    (while probe
      (if (memq (cadr probe) keys)
          (setf (cdr probe) (cdddr probe))
        (setq probe (cddr probe))))
    (cdr head)))

(defun impru--take-exactly (list count)
  "Return exactly COUNT elements from LIST, even if LIST is shorter.
The elements are taken from the front of LIST. If not enough
elements are present in LIST, trailing nils are appended until
the result has exactly COUNT elements."
  (let ((acc '()))
    (while (> count 0)
      (push (pop list) acc)                       ; Trailing nils result naturally from this.
      (setq count (1- count)))
    (nreverse acc)))

(defun impru-present-port (port)
  "Return a string naming the port number PORT."
  (number-to-string port))

(define-error 'impru-invalid-port-string "Port must be a positive integer")

(defun impru-valid-port-string-p (port-str)
  "Return non-nil if PORT-STR names a port number."
  (save-match-data (string-match "\\`[1-9][0-9]*\\'" port-str)))

(defun impru-interpret-port (port)
  (if (impru-valid-port-string-p port)
      (string-to-number port)
    (signal 'impru-invalid-port-string '())))

(cl-defun impru--completing-read
    (prompt completions &optional require-match
            &key history-var default immediate inherit-input-method)
  "Like ‘completing-read’, but with some fluency tweaks.
Many arguments are given using keywords for code readability.
Additionally:

  - If there is only one available completion, then the default
    will be set to it if a default was not already provided. If
    IMMEDIATE is non-nil, then the entire read will be skipped
    and the single value will be returned.

  - If DEFAULT is an integer, it is treated as an index into the
    completion list."
  (catch 'return
    (when-let ((single-completion (and completions (null (cdr completions)) (car completions))))
      (when immediate (throw 'return single-completion))
      (unless default (setq default single-completion)))
    (when (integerp default)
      (setq default (nth default completions)))
    (let ((completion-ignore-case t))
      (completing-read prompt completions nil require-match nil
                       history-var default inherit-input-method))))

(defun impru--positive-prefix-arg-p (arg)
  "Return non-nil if ARG is a positive-like prefix argument.
That means it reflects the user providing either a positive
integer or one or more vaguely positive prefixes (the kind
provided by \\[universal-argument] when no extra input is
given to it)."
  (or (and (integerp arg) (> arg 0))
      (and (listp arg) (integerp (car arg)) (> (car arg) 0))))

(defconst impru--eld-default-fill-column 78)
(defconst impru--eld-default-slogan "Lisp data")

(cl-defun impru--write-eld-file (file-name thunk &key slogan)
  "Write a data file of Lisp values to FILE-NAME.
THUNK is called with ‘standard-output’ and various output control
variables set to reasonable values for round-trip data. It should
print values using the Lisp printer, or any similar facility
allowing them to be read back using the Lisp reader. If THUNK
exits normally, the file FILE-NAME is overwritten with the new
sequence of values, being created if necessary. If it exits
abnormally, the file or absence thereof is unchanged. The return
value is the return value of THUNK.

SLOGAN may be a short human-readable string identifying the type
of file. If it is provided, it will be inserted into the file in
such a way as to be easily visible if the file is manually edited."
  (unless slogan (setq slogan impru--eld-default-slogan))
  (let (result)
    (with-temp-buffer
      (let ((coding-system-for-write 'utf-8-emacs)
            (fill-column impru--eld-default-fill-column)
            (print-circle t) (print-length nil) (print-level nil) (print-quoted t)
            (pp-escape-newlines nil) (print-escape-newlines nil)
            (standard-output (current-buffer)))
        ;; If the user has a recent enough Emacs for ‘lisp-data-mode’ to exist, that's preferred
        ;; for our .eld files. Otherwise, ‘emacs-lisp-mode’ mode is close enough, even though the
        ;; values in the file aren't meant to be evaluated as code.
        (insert (format ";;; %s -*- coding: %s; mode: emacs-lisp; mode: lisp-data -*-\n"
                        slogan coding-system-for-write))
        (setq result (funcall thunk))
        (insert (format "\n;;; End of %s\n" slogan))
        (write-region nil nil file-name)))
    result))

(defmacro impru--with-output-to-eld-file (args &rest body)
  "Write a data file of Lisp values.
ARGS consists of (FILE-NAME . KEYWORD-ARGS). BODY should print
values using the Lisp printer, or any similar facility allowing
them to be read back using the Lisp reader. Provided that it
exits normally, the values are then written to FILE-NAME. See
‘impru--write-eld-file’ for more details."
  (declare (indent 1))
  `(impru--write-eld-file ,(car args) (lambda () ,@body)
                          ,@(cdr args)))

(cl-defun impru--mapc-eld-file (file-name thunk &key before-read-fn at-eof-fn if-does-not-exist)
  "Read a data file of Lisp values from FILE-NAME.
If FILE-NAME exists, values are read from it using the Lisp
reader, and THUNK is called with each value. If BEFORE-READ-FN is
provided, it is called before any values are read, including if
the file contains no values. If AT-EOF-FN is provided, it is
called after all values have been successfully read and
processed, including if the file contained no values. The return
value is the return value of AT-EOF-FN if it was provided, or
t otherwise.

If FILE-NAME does not exist, none of THUNK, BEFORE-READ-FN, or
AT-EOF-FN are called. If IF-DOES-NOT-EXIST is a function, it is
called and its return value is returned. Otherwise, the return
value is IF-DOES-NOT-EXIST, which is nil by default."
  (if (not (file-exists-p file-name))
      ;; The file does not exist.
      (if (functionp if-does-not-exist) (funcall if-does-not-exist)
        if-does-not-exist)
    ;; The file exists.
    (with-temp-buffer
      (insert-file-contents file-name)
      (goto-char (point-min))
      (let ((eof nil) value)
        (when before-read-fn (funcall before-read-fn))
        (while (not eof)
          (setq value (condition-case nil
                          (read (current-buffer))
                        (end-of-file (setq eof t))))
          (unless eof
            (funcall thunk value))))
      (if at-eof-fn (funcall at-eof-fn) t))))

(defmacro impru--do-eld-file-pcase (args &rest clauses)
  "Read a data file of Lisp values.
ARGS consists of (FILE-NAME . KEYWORD-ARGS). CLAUSES are matched
in a way similar to ‘pcase-exhaustive’ once for each Lisp value
read from FILE-NAME; that is, the first matching clause for each
value is processed, or an error is signaled if no clause matches.
See ‘impru--mapc-eld-file’ for more details.

A CLAUSE with a bare keyword as its pattern is treated specially
to produce one of the arguments to ‘impru--mapc-eld-file’:

  - If the file exists, any ‘:before’ clause is run before any
    values are read, and an ‘:after’ clause is run after all
    values are read. These are analogous to ‘:before-read-fn’
    and ‘:at-eof-fn’.

  - If the file does not exist, any ‘:if-does-not-exist’ clause
    is run. This is analogous to the eponymous argument.

An unrecognized bare keyword used as a pattern is an error. If
any of the above keyword arguments is also in KEYWORD-ARGS or
occurs more than once, the behavior is unspecified. If you
actually need to match a keyword by itself, you can quote it
explicitly."
  (declare (indent 1))
  (let ((value-var (gensym "value")) (real-clauses '())
        (saw-obvious-catchall nil)
        before-forms after-forms if-does-not-exist-forms)
    (dolist (clause clauses)
      (pcase-exhaustive clause
        (`(:before . ,forms) (setq before-forms forms))
        (`(:after . ,forms) (setq after-forms forms))
        (`(:if-does-not-exist . ,forms) (setq if-does-not-exist-forms forms))
        (`(,(and (pred keywordp) unrecognized-keyword) . ,_)
         (error "Unrecognized keyword clause: %S" unrecognized-keyword))
        (`(,(pred symbolp) . ,_)
         (setq saw-obvious-catchall t)
         (push clause real-clauses))
        ((pred consp) (push clause real-clauses))
        (_ (error "Ill-formed clause: %S" clause))))
    `(impru--mapc-eld-file
      ,(car args)
      (lambda (,value-var)
        (pcase ,value-var
          ,@(nreverse real-clauses)
          ;; We suppress adding our own error catchall if we saw an obvious catchall clause above.
          ;; Otherwise, ‘pcase’ warns about it distractingly.
          ,@(unless saw-obvious-catchall
              `((_ (error "Unrecognized value in file: %S" ,value-var))))))
      ,@(when before-forms `(:before-read-fn (lambda () ,@before-forms)))
      ,@(when after-forms `(:at-eof-fn (lambda () ,@after-forms)))
      ,@(when if-does-not-exist-forms
          `(:if-does-not-exist (lambda () ,@if-does-not-exist-forms)))
      ,@(cdr args))))

(defun impru--dispatch-buffer-timer (buffer cancel-fn one-shot-p function args)
  (if (not (buffer-live-p buffer))
      (funcall cancel-fn nil)
    (with-current-buffer buffer
      (let (result)
        (unwind-protect
            (setq result (apply function args))
          (when (or result one-shot-p)
            (funcall cancel-fn t)))))))

(defun impru--run-with-buffer-timer
    (seconds repeat function &optional args store-timer-fn buffer)
  "Call a function after some time has passed, in a known buffer.
SECONDS is a number specifying how many seconds after the current
time to wait before calling the function. The return value is a
timer object suitable for ‘cancel-timer’ etc.

When the timer fires, FUNCTION is called as though with (apply
FUNCTION ARGS), with BUFFER as the current buffer. If BUFFER is
dead, then the function is not called at all and the timer is
automatically canceled.

If REPEAT is t, the timer repeats with a period of SECONDS.
If it is a number, it is treated as a period in seconds. For a
repeating timer, the return value of FUNCTION becomes relevant:
the timer is automatically canceled when FUNCTION returns
non-nil.

If STORE-TIMER-FN is provided, it is called with the timer object
after the timer object is created, and it is called with nil when
a one-shot timer is finished or when a repeating timer is canceled
due to the return value of FUNCTION. In both cases, BUFFER is the
current buffer when STORE-TIMER-FN is called. It is not called if
the timer is canceled externally using ‘cancel-timer’, and it is
not called when BUFFER is dead.

If BUFFER is not provided, it defaults to the current buffer as
of the call to ‘impru--run-with-buffer-timer’. Regardless of
this, if BUFFER is not live at the time of the call, an error is
signaled."
  (unless buffer (setq buffer (current-buffer)))
  (unless (buffer-live-p buffer)
    (error "Can't start a buffer timer in a dead buffer"))
  (when (eq repeat t) (setq repeat seconds))
  (unless store-timer-fn (setq store-timer-fn #'ignore))
  (let (timer)
    (setq timer
          (run-with-timer seconds repeat #'impru--dispatch-buffer-timer
                          (or buffer (current-buffer))
                          (lambda (do-store)
                            (cancel-timer timer)
                            (when do-store
                              (funcall store-timer-fn nil)))
                          (null repeat)
                          function args))
    (with-current-buffer buffer
      (funcall store-timer-fn timer))
    timer))


;;;; Interaction buffers

;;;;; Buffer utilities

(defvar impru--backstage-edit-depth nil
  "The recursion depth of the current backstage edit, or nil.
This variable should be let-bound and never set globally. See
‘impru--call-backstage’.")

(defsubst impru--backstagep ()
  "Return non-nil if we are already in a backstage context.
See ‘impru--call-backstage’."
  (equal (recursion-depth) impru--backstage-edit-depth))

(defvar impru--leave-point-at nil
  "Where to leave point after the current backstage context is exited.
This variable should be let-bound and never set globally. See
‘impru--call-backstage’.")

(defun impru--leave-point-at (where)
  "Leave point at WHERE after any backstage context is exited.
If not in a backstage context already, just move point to WHERE.
Otherwise, record the position of WHERE so that point will be
moved there after the backstage context is exited. WHERE must be
an integer or marker; if it is a marker, its position is taken
immediately, regardless of whether point is moved immediately."
  (if (impru--backstagep)
      (setq impru--leave-point-at
            (if (markerp where) (marker-position where) where))
    (goto-char where)))

(defun impru--call-backstage (thunk)
  "Call THUNK in a backstage context.
An Imprū interaction buffer must be the current buffer. If
already in a backstage context, THUNK may be called directly.
Otherwise, a backstage context is established around the call to
THUNK and exited afterwards.

A backstage context binds ‘inhibit-read-only’, saves point, and
temporarily undoes narrowing, leaving an editing state suitable
for operating on an interaction buffer from the perspective of
the buffer or its connection, as opposed to from the perspective
of the user. See the Info node ‘(impru)Backstage context’."
  ;; XXX: doc why recursive edits are Interesting here
  (if (impru--backstagep)
      (funcall thunk)
    (let ((impru--backstage-edit-depth (recursion-depth))
          (impru--leave-point-at nil)
          (inhibit-read-only t))
      (prog1
          (save-excursion
            (save-restriction
              (widen)
              (funcall thunk)
              ))
        (when impru--leave-point-at
          (goto-char impru--leave-point-at))))))

(defmacro impru--backstage-do (&rest body)
  "Evaluate BODY forms in a backstage context.
BODY is evaluated as though with ‘progn’, in a context
established using ‘impru--call-backstage’, which see."
  (declare (indent 0))
  `(impru--call-backstage (lambda () ,@body)))

(defun impru-process-mark (&optional buffer)
  (when-let ((process (get-buffer-process (or buffer (current-buffer)))))
    (process-mark process)))


;;;;; Prompts and input

(defface impru-prompt-face
  '((t))
  "Parent face for prompts in interaction buffers."
  :group 'impru-faces)

(defface impru-inactive-prompt-face
  '((t :inherit (shadow impru-prompt-face)))
  "Face for historical prompts."
  :group 'impru-faces)

(defface impru-active-prompt-face
  '((t :inherit (bold impru-prompt-face)))
  "Face for the active prompt."
  :group 'impru-faces)

(defvar-local impru-prompt-overlay nil
  "Overlay for the active prompt.
If there is no active prompt, this is nil.")

(put 'impru-active-prompt 'text-category-documentation
     "Text category used for the active prompt.
Normally coincides with an overlay accessible via ‘impru-prompt-overlay’.
See also the text category ‘impru-inactive-prompt’.")
(put 'impru-active-prompt 'face 'impru-active-prompt-face)

(put 'impru-inactive-prompt 'text-category-documentation
     "Text category used for historical prompts.
See also the text category ‘impru-active-prompt’.")
(put 'impru-inactive-prompt 'face 'impru-inactive-prompt-face)

;; Common properties for both active and inactive prompts.
(dolist (category '(impru-active-prompt impru-inactive-prompt))
  (put category 'read-only t)
  ;; Don't want these spilling over into the input area.
  (put category 'rear-nonsticky '(category read-only face))
  ;; Don't want insertions before the prompt either.
  (put category 'front-sticky '(read-only)))

(defface impru-input-face
  '((t))
  "Parent face for input in interaction buffers."
  :group 'impru-faces)

(defface impru-inactive-input-face
  '((t :inherit (shadow impru-input-face)))
  "Face for historical input."
  :group 'impru-faces)

(defface impru-active-input-face
  '((t :inherit (bold impru-input-face)))
  "Face for active input."
  :group 'impru-faces)

(defvar-local impru-input-overlay nil
  "Overlay for the active input area.
If there is no active input area, this is nil.")

(put 'impru-input-overlay 'overlay-category-documentation
     "Overlay category used for the eponymous overlay.
See also the text category ‘impru-inactive-input’.")
(put 'impru-input-overlay 'field 'impru-active-input)
(put 'impru-input-overlay 'face 'impru-active-input-face)

(put 'impru-inactive-input 'text-category-documentation
     "Text category used for historical input.
See also the overlay category ‘impru-input-overlay’.")
(put 'impru-inactive-input 'read-only t)
(put 'impru-inactive-input 'field 'impru-input)
(put 'impru-inactive-input 'face 'impru-inactive-input-face)
;; Don't allow insertions at the beginning of historical input. Prompts have read-only set as
;; rear-nonsticky, so if we don't apply read-only to the front then an editable gap remains.
(put 'impru-inactive-input 'front-sticky '(read-only))

(put 'impru-input-terminator 'text-category-documentation
     "Text category used for terminating newlines after inputs.
These are used to force the input field to terminate.")
(put 'impru-input-terminator 'field 'impru-input-terminator)
(put 'impru-input-terminator 'read-only t)

(defun impru-inactivate-prompt ()
  "Inactivate the active prompt, if any.
If there was none, silently do nothing. The text is not deleted.
After this returns, there is no active prompt."
  (when impru-prompt-overlay
    (impru--backstage-do
      (progn ;with-silent-modifications
        (add-text-properties
         (overlay-start impru-prompt-overlay) (overlay-end impru-prompt-overlay)
         '(category impru-inactive-prompt)))
      (delete-overlay impru-prompt-overlay)
      (setq impru-prompt-overlay nil))))

(defun impru-inactivate-input ()
  "Inactivate the active input area, if any.
If there was none, silently do nothing. The text is not deleted
and is instead given properties suitable for historical input,
including appending an input terminator newline if appropriate.
After this returns, there is no active input area."
  (when impru-input-overlay
    (impru--backstage-do
      (progn ;with-silent-modifications
        (add-text-properties
         (overlay-start impru-input-overlay) (overlay-end impru-input-overlay)
         '(category impru-inactive-input)))
      (delete-overlay impru-input-overlay)
      (setq impru-input-overlay nil))))

(defun impru-insert-input-terminator~ ()
  "Insert an input terminator at point.
This is a newline with the category ‘impru-input-terminator’."
  (insert ?\n)
  (add-text-properties (1- (point)) (point) '(category impru-input-terminator)))

(defun impru-at-input-terminator-p ()
  "Return non-nil if point is right before an input terminator.
This is a newline with the category ‘impru-input-terminator’."
  (and (not (eobp)) (= (char-after) ?\n)
       (eq (get-text-property (point) 'category) 'impru-input-terminator)))

(defun impru-capture-input-inactivate (&optional no-newline)
  "Capture the current input text and inactivate the prompt and input.
Return the input text as a string, including a terminating
newline unless NO-NEWLINE is non-nil. The text is not deleted
from the buffer, but it may be rearranged to be suitable for
inclusion in the history part of the buffer.

If the process mark was right behind the active prompt, it is
moved to after the newly historicalized region of text."
  (impru--backstage-do
    (when impru-input-overlay
      (let* ((beg (overlay-start impru-input-overlay))
             (end (overlay-end impru-input-overlay))
             (mark (impru-process-mark))
             (should-move-mark
              (and impru-prompt-overlay mark
                   (= mark (overlay-start impru-prompt-overlay)))))
        (goto-char end)
        (if (impru-at-input-terminator-p)
            (goto-char (1+ (point)))
          (impru-insert-input-terminator~))
        (when should-move-mark
          (set-marker mark (point)))
        (prog1 (buffer-substring-no-properties
                beg (if no-newline (1- (point)) (point)))
          (impru-inactivate-prompt)
          (impru-inactivate-input))))))

(defun impru-refresh-prompt (&optional force)
  "Insert an active prompt and input area at the end of the buffer.
If there is already an active prompt, silently do nothing unless
FORCE is non-nil.

If this inserts a new prompt, the process mark is also updated so
that new output will be inserted before the new prompt."
  (when (or force (not impru-prompt-overlay))
    (impru--backstage-do
      (goto-char (point-max))
      (unless (bolp) (insert ?\n))
      (when-let ((mark (impru-process-mark)))
        (unless (marker-position mark)
          (set-marker mark (point))
          ;; This is needed so that it doesn't advance when we insert the prompt.
          (set-marker-insertion-type mark nil)))
      (let ((prompt-start (point)))
        (insert ">> ")
        ;; Prompt overlay: absorb inserted text behind, but not in front.
        (setq impru-prompt-overlay (make-overlay prompt-start (point) nil t nil))
        (overlay-put impru-prompt-overlay 'category 'impru-prompt-overlay)
        (add-text-properties prompt-start (point) '(category impru-active-prompt)))
      (let ((input-start (point)))
        (impru-inactivate-input)
        ;; Input overlay: absorb inserted text in front, but not behind.
        (setq impru-input-overlay (make-overlay input-start input-start nil nil t))
        (overlay-put impru-input-overlay 'category 'impru-input-overlay)
        (impru--leave-point-at input-start)))))


;;;;; Connection management

(define-error 'impru-not-connected "Not connected")

(defvar-local impru-connspec nil
  "The effective connspec for this buffer.")
(defvar-local impru-connection-state nil
  "The overall state of the connection in this buffer.
This is one of the following symbols:

  nil -- closed
  pending -- being established
  open -- normally open
  closing -- being torn down

This should be non-nil when and only when there is an interaction
process attached; this is normally ensured by the sentinel.")

;; We also set the ‘impru-protocol-stack’ variable buffer-locally.

(defun impru--get-process ()
  "Return the process for this interaction buffer, or nil if none."
  (get-buffer-process (current-buffer)))

(defun impru--check-connection-open ()
  "Signal an error if the connection is not in a normal open state.
This includes if ‘impru-connection-state’ is ‘pending’ or ‘closing’
even though a connection process exists. If this returns normally,
it returns the connection process, which is always non-nil. The
error signaled if the connection is not in a normal open state is
an ‘impru-not-connected’ error."
  (if-let ((open (eq impru-connection-state 'open))
           (process (impru--get-process)))
      process
    (signal 'impru-not-connected '())))

(defconst impru-default-receive-face '(:inherit )
  "The default list of face properties to apply to received text.")
(defvar-local impru--current-receive-face nil
  "The list of face properties to apply to received text.")

(defun impru--upwardmost-send (p1 p2 p3)
  "Called on the top side of an interaction buffer's protocol stack.
Strings are sent to the underlying process if the connection is
open. Anything else is ignored. The interaction buffer must be
the current buffer."
  (when (stringp p1)
    (process-send-string (impru--check-connection-open)
                         (substring p1 p2 p3))))

(defun impru--downwardmost-received (p1 p2 p3)
  "Called on the bottom side of an interaction buffer's protocol stack.
Strings are inserted into the buffer with suitable fontification.
Some other out-of-band messages are also accepted. The interaction
buffer must be the current buffer, and point should already be
somewhere suitable for output."
  (pcase p1
    ((and (pred stringp) s)
     (insert (propertize (substring s p2 p3) 'face impru--current-receive-face)))
    ;; TODO: doc the accepted out-of-band messages
    (`(vt sgr-face . ,props)
     (setq impru--current-receive-face (or props impru-default-receive-face)))))

(defun impru-send-string (string)
  "Send exactly STRING to the current connection.
This does not automatically append a newline."
  (impru-protocol-stack-encode impru-protocol-stack string))

(defun impru-send-input ()
  "Send the active input line to the current connection.
The prompt and input become part of the history, and a new active
prompt and input area are set up."
  (interactive)
  (let ((process (impru--check-connection-open)))
    (impru--backstage-do
      (when-let ((input (impru-capture-input-inactivate)))
        (impru-send-string input))
      (impru-refresh-prompt)
      (impru--leave-point-at (overlay-start impru-input-overlay)))))

(defvar impru-process-serial-number 0
  "A serial number used for uniquifying connection process names.
This variable stores the last number used.")

(defun impru-connspec-process-name (connspec)
  "Return a suitable process name for CONNSPEC."
  (setq impru-process-serial-number (1+ impru-process-serial-number))
  (with-output-to-string
    (princ "impru#")
    (princ impru-process-serial-number)
    (when-let ((host (plist-get connspec :host)))
      (write-char ?:)
      (princ host)
      (when-let ((port (plist-get connspec :port)))
        (write-char ?:)
        (princ port)))))

(defun impru-connspec-label (connspec)
  "Return a suitable display label for CONNSPEC."
  (with-output-to-string
    (when-let ((login (plist-get connspec :login)))
      (princ login)
      (write-char ?@))
    (if-let ((world-name (plist-get connspec :world-name)))
        (princ world-name)
      (if-let ((host (plist-get connspec :host)))
          (progn
            (princ host)
            (when-let ((port (plist-get connspec :port)))
              (write-char ?:)
              (princ port)))
        (princ "(unknown)")))))

(defun impru-connspec-buffer-name (connspec)
  "Return a suitable buffer name for CONNSPEC."
  (concat "*Imprū: " (impru-connspec-label connspec) "*"))

(defun impru--call-as-process-callback (process thunk)
  "Call THUNK in a manner suitable for a callback for PROCESS.
If PROCESS has a live buffer attached to it, THUNK is called
backstage with that buffer current. If there is no buffer, THUNK
is not called at all, under the assumption that the process is
going to go away and further callbacks can be ignored."
  (when-let ((buffer (process-buffer process)))
    (when (and buffer (buffer-live-p buffer))
      (with-current-buffer buffer
        (impru--call-backstage thunk)))))

(defmacro impru--for-process-callback (process &rest body)
  "Execute BODY in a manner suitable for a callback for PROCESS.
This just wraps ‘impru--call-as-process-callback’."
  (declare (indent 1))
  `(impru--call-as-process-callback ,process (lambda () ,@body)))

(defun impru--connection-process-filter (process data)
  "Filter for interaction processes.
See ‘set-process-filter’. This mostly inserts output into the buffer
as appropriate."
  (unless (string-empty-p data)
    (impru--for-process-callback process
      (let ((mark (process-mark process)))
        (unless (marker-position mark)
          (set-marker mark
                      (if impru-prompt-overlay
                          (overlay-start impru-prompt-overlay)
                        (point-max))))
        (let ((start (marker-position mark)))
          (set-marker-insertion-type mark t)
          (goto-char start)
          ;; This inserts output as appropriate using ‘impru--downwardmost-received’.
          (impru-protocol-stack-decode impru-protocol-stack data)
          (set-marker-insertion-type mark nil)
          ;; We use an artificial EOL between the process mark and the active prompt
          ;; to keep the prompt on its own line.
          (if (or (bobp) (= (char-before) ?\n))
              ;; After natural EOL; delete any artificial EOL.
              (when (and (not (eobp)) (= (char-after) ?\n))
                (delete-char 1))
            ;; Not after natural EOL; add an artificial EOL.
            (unless (or (eobp) (= (char-after) ?\n))
              (insert ?\n)))
          ;; Any inserted text is marked as history text. TODO: use a category?
          (add-text-properties start (point) '(read-only t front-sticky (read-only))))))))

;; Compatibility alias.
(defalias 'impru--connection-output-filter 'impru--connection-process-filter)

(defun impru-message (options format &rest args)
  "Deliver a message to the user as though with ‘message’.
OPTIONS is currently ignored and ‘message’ called every time.
However, in the future, this should do something clever involving
adding the message to the connection history, not using the echo
area if the buffer isn't visible, etc. etc.

This should _usually_ only be called from an interaction buffer,
but it is not invalid to call it otherwise."
  (apply #'message format args))

(defvar-local impru--pending-status-timer nil
  "Repeating timer for rechecking the status of a pending connection.
Uses the function ‘impru--recheck-pending-status’.")

(defun impru--recheck-pending-status ()
  "Buffer timer function for rechecking the status of a pending connection.
Returns non-nil if no further timed rechecks are needed."
  (if-let ((process (get-buffer-process (current-buffer)))
           (status (process-status process)))
      (unless (eq status 'pending)
        (prog1 t
          (impru--backstage-do
            (impru--handle-status~ status nil))))
    t))

(defun impru--ensure-pending-recheck~ ()
  "Ensure ‘impru--pending-status-timer’ is running in this buffer."
  (unless impru--pending-status-timer
    (impru--run-with-buffer-timer
     1 t #'impru--recheck-pending-status '()
     (lambda (timer) (setq impru--pending-status-timer timer)))))

(defun impru--handle-pending~ (status &optional desc)
  "Handle a notification that a connection is being attempted.
STATUS and DESC are per ‘impru--handle-status~’."
  (setq impru-connection-state 'pending)
  (impru-message nil "Waiting for connection...")
  ;; We _should_ get all state changes, but somehow it seems like the connection can fail
  ;; "immediately" and then not call our sentinel, so. Sigh. TODO: when did that happen?
  (impru--ensure-pending-recheck~))

(defun impru--handle-open~ (status &optional desc)
  "Handle the successful opening of a connection.
STATUS and DESC are per ‘impru--handle-status~’."
  (unless (eq impru-connection-state 'open)
    (setq impru-connection-state 'open)
    (impru-message nil "Connection established.")
    ;; Note that we have to call ‘impru-refresh-prompt’ before anything that might do a recursive
    ;; edit, because the process filter is already running and will expect the buffer to be in the
    ;; right layout for an open connection. However, we should really be avoiding recursive edits
    ;; from in the middle of a process sentinel anyway; do anything heavy in the bottom half.
    (impru-refresh-prompt)

    ;; Queue bottom half of just-opened connection handling to execute in the main input loop, so
    ;; we're not doing it from inside a process sentinel or some other unusual context.
    (impru--run-with-buffer-timer
     ;; TODO: this should be a hook
     0 nil (lambda () (impru-attempt-auto-login)))))

(defun impru--handle-close~ (status &optional desc)
  "Handle a connection close.
STATUS and DESC are per ‘impru--handle-status~’."
  (when impru-connection-state
    (cl-case status
      (closed (impru-message nil "Connection closed."))
      (failed (impru-message nil "Connection failed."))
      ((deleted exit)
       ;; If we deleted the process locally, then what actually happened depends on what we were
       ;; doing at the time. The user could also have just forcibly deleted it, of course.
       (cl-case impru-connection-state
         (closing (impru-message nil "Connection closed."))
         (pending (impru-message nil "Connection aborted."))
         (otherwise (impru-message nil "Connection lost."))))
      (otherwise (impru-message nil "Connection lost."))))
  (setq impru-connection-state nil)
  (cond
   ((and impru-input-overlay
          (/= (overlay-start impru-input-overlay)
              (overlay-end impru-input-overlay)))
    ;; Nonempty input; leave it in the history.
    (impru-inactivate-prompt)
    (impru-inactivate-input))
   (t
    ;; Delete trailing prompt entirely.
    (when impru-prompt-overlay
      (let ((beg (overlay-start impru-prompt-overlay))
            (end (overlay-end impru-prompt-overlay)))
        (delete-overlay impru-prompt-overlay)
        (setq impru-prompt-overlay nil)
        (delete-region beg end)))
    (when impru-input-overlay
      (delete-overlay impru-input-overlay)
      (setq impru-input-overlay nil))))
  ;; Just a little finishing touch.
  (set-buffer-modified-p nil))

(defun impru--handle-strange-status~ (status &optional desc)
  (warn "Unrecognized status: %S" status))

(defvar-local impru-last-process-status nil
  "The last handled status for this buffer's process.
Used by the process sentinel via ‘impru--handle-status~’.")

(defun impru--handle-status~ (status &optional desc)
  "Perform actions appropriate to our process now having status STATUS.
STATUS is a symbol; ‘process-status’ lists most possibilities,
but the symbol ‘deleted’ is also accepted for when the process
has just been deleted locally.

DESC may be a string description received by the process
sentinel, if any; it should be treated as very loosely typed.

This uses ‘impru-last-process-status’ to debounce updates."
  (unless (eq status impru-last-process-status)
    (cl-case status
      ((connect) (impru--handle-pending~ status desc))
      ((open run) (impru--handle-open~ status desc))
      ((closed deleted failed signal exit)
       (impru--handle-close~ status desc))
      (otherwise (impru--handle-strange-status~ status desc)))
    (setq impru-last-process-status status)))

(defun impru--connection-process-sentinel (process desc)
  "Sentinel for interaction processes.
See ‘set-process-sentinel’."
  (impru--for-process-callback process
    ;; DESC is weird. We _mostly_ ignore it, but we can't distinguish a normal remote close from a
    ;; local process deletion with just the result of ‘process-status’. Process deletion is how we
    ;; handle local closes of several types, so those are distinguished above.
    (let ((status (if (equal desc "deleted\n") 'deleted (process-status process))))
      (impru--handle-status~ status desc))))

(defun impru-reset-connection-state (connspec)
  "Reset connection-specific state in this interaction buffer.
‘impru-connspec’ is set unconditionally to CONNSPEC, and other
state variables are prepared accordingly. Stray timers and so
forth may also be cleaned up.

Notably, ‘impru-protocol-stack’ is set to nil; it is expected
that any code handling a new connection will set up the stack
after this if appropriate."
  (when impru--pending-status-timer
    (cancel-timer impru--pending-status-timer))
  (setq-local impru-connspec connspec
              impru-connection-state nil
              impru-protocol-stack nil
              impru-last-process-status nil
              impru--pending-status-timer nil
              impru--current-receive-face impru-default-receive-face))

(defun impru--connspec-tls-parameters (connspec)
  "Return TLS parameters suitable for CONNSPEC.
The result is something you would pass as a ‘:tls-parameters’
argument to ‘open-network-stream’, which see."
  (cons 'gnutls-x509pki
        (gnutls-boot-parameters :hostname (plist-get connspec :host))))

(defun impru--open-network-stream (&optional connspec)
  "Open a connection in the current interaction buffer.
CONNSPEC is the connection specification to use; if it is not
specified, it defaults to the value of ‘impru-connspec’. Any
existing process is _not_ cleaned up first."
  (or connspec (setq connspec impru-connspec)
      (error "No connection information"))
  (impru-reset-connection-state connspec)

  (let ((stack (make-impru-protocol-stack
                #'impru--upwardmost-send #'impru--downwardmost-received)))
    ;; TODO: don't hardcode the stack
    (impru-protocol-stack-push stack 'telnet)
    (impru-protocol-stack-push stack 'sgr)
    (impru-protocol-stack-push stack 'coding-system 'utf-8-dos)
    (impru-protocol-stack-push stack 'mcp)
    (setq-local impru-protocol-stack stack))

  ;; XXX: it seems like GnuTLS verification can enter a recursive edit and doesn't stop
  ;; the network process in the meantime aaa?
  (let* ((tls (plist-get connspec :tls))
         (process (open-network-stream 
                   (impru-connspec-process-name connspec)
                   (current-buffer) (plist-get connspec :host) (plist-get connspec :port)
                   :type (if tls 'tls 'plain)
                   :tls-parameters (and tls (impru--connspec-tls-parameters connspec))
                   :nowait t)))
    (set-process-coding-system process 'no-conversion 'no-conversion)
    (set-process-filter process #'impru--connection-process-filter)
    (set-process-sentinel process #'impru--connection-process-sentinel)
    (set-process-query-on-exit-flag process t)
    (impru--handle-status~ (process-status process))))

(defun impru-disconnect ()
  "Disconnect the current connection, if present.
Signals an ‘impru-not-connected’ error otherwise."
  (interactive)
  (when impru-connection-state
    (setq impru-connection-state 'closing))
  (if-let ((process (impru--get-process)))
      (delete-process process)
    (signal 'impru-not-connected '())))

(defun impru-reconnect ()
  "Reconnect the current connection, disconnecting first if needed."
  (interactive)
  (when impru-connection-state
    (impru-disconnect))
  (cl-assert (null impru-connection-state))
  (impru--open-network-stream))


;;;;; Access to backing world parameters

(declare-function impru--borrow-user-world "impru"
                  (world-name &optional alist-var))

(defun impru--call-updating-current-world (thunk)
  (let ((world-name (plist-get impru-connspec :world-storage-name)))
    (if (null world-name)
        (error "No world to update")  ;TODO: possibly prompt to save later
      (let ((worldspec (impru--borrow-user-world world-name))
            (save-new-worldspec-fn (lambda (new-worldspec)
                                     (impru--replace-user-world world-name new-worldspec)
                                     (impru-save-user-world world-name))))
        (funcall thunk worldspec save-new-worldspec-fn)))))

(defmacro impru--updating-current-world (vars &rest body)
  (declare (indent 1))
  `(impru--call-updating-current-world (lambda ,vars ,@body)))


;;;;; Credential management

(defvar-local impru-save-credentials-function nil
  "A transient function usable for saving recent credentials, or nil.
When a token is used for auto-login, this variable may be set
from its ‘:save-function’ property. If non-nil, the value is a
function of no arguments which begins the save process when
called. The function will usually prompt the user first, and may
interact further with the user as necessary. If the function
returns normally, it is not called again.")

(defun impru-persist-password-ref (login-name new-password-ref)
  "Save NEW-PASSWORD-REF back to the LOGIN-NAME login record for this world.
The login record in the worldspec is updated and any previous
password ref is overwritten. The new worldspec is saved."
  (setq impru-connspec (plist-put impru-connspec :password-ref new-password-ref))
  (impru--updating-current-world (worldspec save-new-worldspec-fn)
    (when-let ((logins (copy-alist (plist-get worldspec :logins)))
               (this-record (assoc login-name logins)))
      (setcdr this-record
              (plist-put (copy-sequence (cdr this-record))
                         :password-ref new-password-ref))
      (funcall save-new-worldspec-fn
               (plist-put (copy-sequence worldspec) :logins logins)))))

(defun impru--dereference-password-ref (password-ref)
  "Return a token extracted from PASSWORD-REF, or nil.
If PASSWORD-REF is recognizable as a password ref (per the Info
node ‘(impru)Credentials internals’), attempt to dereference it
and return the associated token. Return nil if PASSWORD-REF is
recognizably not a supported password ref. If the dereference
function signals an error, it is unspecified whether an error
is signaled or whether this returns nil."
  (and (proper-list-p password-ref)
       (symbolp (car password-ref))
       (let ((arity (function-get (car password-ref) 'impru-password-ref-function-arity)))
         (and (consp arity) (integerp (car arity)) (integerp (cdr arity))
              (let* ((context-args (list impru-connspec))
                     (specific-args (cdr password-ref))
                     (final-args (append (impru--take-exactly context-args (car arity))
                                         (impru--take-exactly specific-args (cdr arity)))))
                (apply (car password-ref) final-args))))))

(declare-function impru-auth-source-dereference-password "impru"
                  (connspec &optional transient))

(defun impru--find-auto-login-credentials (connspec)
  "Try to find a token for automatic login for CONNSPEC.
If CONNSPEC already has a ‘:password-ref’ property, the password
ref is dereferenced. Otherwise, unspecified ambient sources may
be searched for suitable credentials corresponding to CONNSPEC."
  (if-let ((password-ref (plist-get connspec :password-ref)))
      ;; We have a source we're expected to use.
      (or (impru--dereference-password-ref password-ref)
          (prog1 nil
            (impru-message nil "Couldn't fetch credentials")))
    ;; No idea, so check various sources.
    (or (and (impru-use-auth-source t)
             (when-let ((creds (impru-auth-source-dereference-password connspec t)))
               (and (y-or-n-p (format "Use auth-source credentials for %s? "
                                      (plist-get connspec :login)))
                    creds))))))

(defun impru-attempt-auto-login ()
  "Attempt automatic login in this buffer.
If credentials are found, a Tiny-style ‘connect’ line is sent to
the server. If the credentials were transient, the user may be
able to save them with the command ‘impru-save-credentials’. If
no credentials are found, nothing happens."
  (when-let ((creds (impru--find-auto-login-credentials impru-connspec))
             (login (plist-get creds :login)) (password (plist-get creds :password)))
    (impru--backstage-do
      (when (setq impru-save-credentials-function (plist-get creds :save-function))
        (impru--run-with-buffer-timer
         2 nil (lambda ()
                 (message "Use %s to save credentials"
                          (substitute-command-keys "\\[impru-save-credentials]")))))
      (impru-message nil "Logging in as %s..." login)
      ;; XXX: hardcoded Tiny-style login; also, eventually want to put the line in history?
      (impru-send-string (format "connect %s %s\n" login password)))))

(defun impru-save-credentials ()
  "Save transient credentials that were used for automatic login.
This calls the value of ‘impru-save-credentials-function’ if it
is non-nil."
  (interactive)
  (if (functionp impru-save-credentials-function)
      (when (funcall impru-save-credentials-function)
        (message "Credentials saved")
        (setq impru-save-credentials-function nil))
    (message "(No credentials to save)")))


;;;;;; Read-only auth-source support

(defun impru-auth-source--connspec-search-plist (connspec)
  "Return a plist suitable for searching ‘auth-source’ based on CONNSPEC.
If not enough properties are available to construct a search,
return nil."
  (when-let ((host (plist-get connspec :host))
             (port (plist-get connspec :port))
             (login (plist-get connspec :login)))
    (list :host host :port (number-to-string port) :user login)))

(defun impru-auth-source--parse-token (token)
  "Convert an ‘auth-source’ token into an Imprū token.
If the token is not in the expected format, return nil. Any
function-boxed secret in the ‘auth-source’ token is unboxed in
the process."
  (when-let ((login (plist-get token :user)))
    (let ((password (plist-get token :secret)))
      (while (functionp password)
        (setq password (funcall password)))
      (and (stringp password)
           (list :login login :password password)))))

(defun impru-auth-source--find-credentials (connspec)
  "Search for credentials for CONNSPEC via ‘auth-source’.
If credentials are available, return them as an Imprū token. If
the search cannot be constructed or no credentials of a suitable
format are available, return nil"
  (when-let ((search-plist (impru-auth-source--connspec-search-plist connspec))
             (token (car (apply #'auth-source-search :require '(:secret) :max 1 search-plist))))
    (impru-auth-source--parse-token token)))

(defun impru-auth-source--prompt-to-persist (connspec)
  "Prompt the user to persist the use of ‘auth-source’ for CONNSPEC.
If the user assents, a password ref is stored back so that future
logins will search ‘auth-source’. Any secret is not copied."
  (when (y-or-n-p (format "Keep using password from auth-source for %s? "
                          (impru-connspec-label connspec)))
    (prog1 t
      (impru-persist-password-ref
       (plist-get connspec :login) '(impru-auth-source-dereference-password)))))

;; Has a forward declaration above.
(defun impru-auth-source-dereference-password (connspec &optional transient)
  "Dereference an intention to search ‘auth-source’ regarding CONNSPEC.
Return an Imprū token or nil. If TRANSIENT is non-nil, any
returned token will have a suitable ‘:save-function’ property.
Uses ‘impru-auth-source--find-credentials’ underneath.

A reference to this password dereferencing function may be stored
as part of a password ref in a login record."
  (when-let (creds (impru-auth-source--find-credentials connspec))
    (nconc
     (and transient
          (list :save-function (lambda () (impru-auth-source--prompt-to-persist connspec))))
     creds)))
(put 'impru-auth-source-dereference-password 'impru-password-ref-function-arity '(1 . 0))

(impru-defcompat mnrcr
  'mnrcr-auth-source-dereference-password
  'impru-auth-source-dereference-password)


;;;;; Command dispatchers (mostly stubs right now)

(defun impru-read-client-command ()
  nil)

(defun impru-execute-client-command (command)
  (interactive (list (impru-read-client-command)))
  (error "Don't actually have client commands yet"))

(defun impru-execute-client-command-or-self-insert ()
  (interactive)
  (if (and impru-input-overlay
           (= (point) (overlay-start impru-input-overlay)))
      (call-interactively #'impru-execute-client-command)
    (call-interactively #'self-insert-command)))

(defun impru-read-world-command ()
  nil)

(defun impru-execute-world-command (command)
  (interactive (list (impru-read-world-command)))
  (error "Don't actually have world commands yet"))

(defun impru-execute-world-command-or-self-insert ()
  (interactive)
  (if (and impru-input-overlay
           (= (point) (overlay-start impru-input-overlay)))
      (call-interactively #'impru-execute-world-command)
    (call-interactively #'self-insert-command)))


;;;;; Major mode definition and entrypoints

(define-derived-mode impru-interaction-mode fundamental-mode "Imprū"
  "Major mode for interacting with a virtual world using Imprū."
  (setq-local scroll-conservatively 101
              ansi-color-apply-face-function #'ansi-color-apply-text-property-face
              impru-protocol-stack nil))

(let ((map impru-interaction-mode-map))
  (define-key map [return] #'impru-send-input) ; impru-send-input-or-...?
  (define-key map [?\C-j] #'impru-send-input)
  (define-key map [?\C-m] #'impru-send-input)
  (define-key map [?\M-j] #'impru-send-input)
  (define-key map [?\C-c ?:] #'impru-execute-client-command)
  (define-key map [?/] #'impru-execute-client-command-or-self-insert)
  ;; world commands are based on world type and/or MCP enhancements and such
  (define-key map [?\C-c ?\;] #'impru-execute-world-command)
  (define-key map [?\\] #'impru-execute-world-command-or-self-insert)
  (define-key map [?\C-c ?\C-c ?\C-d] #'impru-disconnect)
  (define-key map [?\C-c ?\C-c ?\C-r] #'impru-reconnect)
  (define-key map [?\C-c ?\C-c ?\C-s] #'impru-save-credentials))

;;;###autoload
(defun impru-open-connection (connspec &optional buffer)
  "Open a connection to a text world using Imprū.
CONNSPEC is the connection specification determining where to
connect. If BUFFER is provided, the connection is opened in
BUFFER, which is made into an Imprū interaction buffer;
otherwise, a suitable buffer is found or created, then switched
to using ‘switch-to-buffer’.

When called interactively, the user is prompted for a host and
port. If a positive or vague prefix argument is given, the
connection is encrypted with TLS; otherwise, it is not encrypted.

If the buffer provided or found is already connected to a world,
the behavior is currently unspecified, but it should really do
something more clever."
  (interactive
   (let ((connspec (list :tls (impru--positive-prefix-arg-p current-prefix-arg))))
     (setq connspec (plist-put connspec :host (read-string "Host: ")))
     (setq connspec (plist-put connspec :port (impru-interpret-port (read-string "Port: "))))
     (list connspec)))
  (unless buffer
    (setq buffer (get-buffer-create (impru-connspec-buffer-name connspec)))
    (switch-to-buffer buffer))
  (with-current-buffer (or buffer (setq buffer (current-buffer)))
    (unless (eq major-mode 'impru-interaction-mode)
      (impru-interaction-mode))
    (when (impru--get-process)
      (impru-disconnect))
    (impru--open-network-stream connspec)))

;;;###autoload (autoload 'mnrcr-open-connection "impru")
(impru-defcompat mnrcr 'mnrcr-open-connection 'impru-open-connection)


;;;; Persistent world management

;;;;; World types

;; Not much here yet.
(defvar impru-world-type-alist
  '((muck :label "MUCK")
    (moo :label "MOO")
    (mush :label "MUSH")
    (nil :label "Unspecified")))


;;;;; User worlds

(defvar impru-saved-user-world-alist '()
  "Alist of file-loaded user worlds, keyed by name symbol.
This is similar to ‘impru-user-world-alist’, but it allows for
making transient modifications to some worlds while saving
others.

This is meant to always be in sync with the underlying file, but
the buffer management currently doesn't do that. XXX")

(defvar impru-user-world-alist '()
  "Alist of user-configured worlds, keyed by name symbol.
The ‘cdr’ of each entry is the world plist.")

(defun impru--copy-world-alist (alist)
  "Copy an alist of worldspecs, keyed by name symbol.
Neither the copied alist nor the worldspecs in it share direct
structure with their counterparts. Values of properties in the
worldspecs may be shared."
  (mapcar #'copy-sequence alist))

(defsubst impru--borrow-user-world (world-name &optional alist-var)
  "Borrow the worldspec for a configured world.
It remains valid at least until any operation that modifies or
replaces the same world through the same alist. If the world does
not exist, nil is returned.

If ALIST-VAR is specified, it names a variable which is accessed
instead of ‘impru-user-world-alist’."
  (alist-get world-name (symbol-value (or alist-var 'impru-user-world-alist))))

(defsubst impru--get-user-world (world-name &optional alist-var)
  "Return the worldspec for a configured world as a fresh plist.
If the world does not exist, nil is returned.

If ALIST-VAR is specified, it names a variable which is accessed
instead of ‘impru-user-world-alist’."
  (copy-sequence (impru--borrow-user-world world-name alist-var)))

(defsubst impru--replace-user-world (world-name worldspec &optional alist-var)
  "Replace or provide the worldspec for a configured world.
Takes ownership of WORLDSPEC, which should not be separately
modified afterwards. WORLD-NAME becomes a configured world name
if it was not one already.

If ALIST-VAR is specified, it names a variable which is accessed
instead of ‘impru-user-world-alist’."
  (setf (alist-get world-name
                   (symbol-value (or alist-var 'impru-user-world-alist)))
        worldspec))

(defsubst impru--delete-user-world (world-name &optional alist-var)
  "Delete a configured world.
WORLD-NAME ceases to be a configured world name. If it was not
one already, nothing happens and no error is signaled.

If ALIST-VAR is specified, it names a variable which is accessed
instead of ‘impru-user-world-alist’."
  (unless alist-var (setq alist-var 'impru-user-world-alist))
  (set alist-var (assq-delete-all world-name (symbol-value alist-var))))

(defconst impru-default-user-world-directory "impru/")

(defcustom impru-user-world-directory impru-default-user-world-directory
  "Directory in which to store Imprū world files.
If this is not an absolute name, it is interpreted relative to
‘user-emacs-directory’.

If this is left at its default value and the directory does not
exist, compatibility locations may also be searched for world
files."
  :group 'impru
  :type 'directory)

(defconst impru-world-index-file-name "world-index.eld"
  "The name of the world index file to access.
This is relative to the user world directory, which
is configured using ‘impru-user-world-directory’;
see ‘impru-get-user-world-directory’ for more info.")

;; TODO: need to be able to autoload these individual files later.
;(defconst impru-world-file-name "%s.world")

(defun impru--find-compat-user-world-directory ()
  "Find an existing user world directory under an old default name.
Return the expanded directory name, or nil if none was found. May
test the directories using any heuristic, not just plain existence.

This helper function is used by ‘impru-get-user-world-directory’."
  (let ((compat-full-names '()))
    (impru-whencompat mnrcr
      (push (expand-file-name "mnrcr/" user-emacs-directory) compat-full-names))
    (seq-find (lambda (dir-name)
                (and (file-directory-p dir-name)
                     (file-exists-p (expand-file-name impru-world-index-file-name dir-name))))
              compat-full-names)))

(defun impru-get-user-world-directory (&optional createp)
  "Return the directory to use for storing user worlds.
This is configured through ‘impru-user-world-directory’,
but compatibility locations may also be checked if that
variable is left at its default value.

If no suitable directory is found, and CREATEP is non-nil,
a suitable directory is created and its name is returned;
an error is signaled if the directory cannot be created.
If CREATEP is nil, then nil is returned instead."
  (let ((full-name (expand-file-name impru-user-world-directory user-emacs-directory))
        compat-full-name)
    (cond
     ((file-directory-p full-name)
      (file-name-as-directory full-name))
     ((and (equal impru-user-world-directory impru-default-user-world-directory)
           (setq compat-full-name (impru--find-compat-user-world-directory)))
      (file-name-as-directory compat-full-name))
     ((and createp (prog1 t (make-directory full-name t)))
      (file-name-as-directory full-name))
     (t nil))))

(defun impru--write-world-index (file-name alist)
  "Write a world index file FILE-NAME containing world data from ALIST."
  (impru--with-output-to-eld-file (file-name :slogan "Imprū world index file")
    (dolist (entry impru-user-world-alist)
      (pp (cons 'impru-world (cdr entry))))))

(defun impru-save-user-worlds (&optional show-messages)
  "Commit and save all user world definitions.
Copy definitions from ‘impru-user-world-alist’ to
‘impru-saved-user-world-alist’, overwriting all saved
definitions, then write them to the user world index file.

If SHOW-MESSAGES is non-nil, progress messages may be displayed."
  (interactive)
  (let ((file-name (concat (impru-get-user-world-directory t) impru-world-index-file-name)))
    (setq impru-saved-user-world-alist (impru--copy-world-alist impru-user-world-alist))
    (impru--write-world-index file-name impru-saved-user-world-alist)))

(defun impru-save-user-world (world-name)
  "Commit and save the user world definition for WORLD-NAME.
Copy the definition for that world from ‘impru-user-world-alist’
to ‘impru-saved-user-world-alist’, overwriting any saved
definition for that world name, then write it to the user world
index file. It is unspecified whether only the relevant part of
the file is altered or whether it is overwritten with all saved
definitions."
  (let ((file-name (concat (impru-get-user-world-directory t) impru-world-index-file-name)))
    ;; TODO: use map-* more elsewhere.
    ;; This correctly uses alist semantics if the target is nil...
    (setf (map-elt impru-saved-user-world-alist world-name)
          (impru--get-user-world world-name))
    (impru--write-world-index file-name impru-saved-user-world-alist)))

(defun impru-import-worldspec (worldspec)
  "Convert WORLDSPEC into a current-format worldspec and return it.
This is used for reading world index files that may be using
older formats; see the source code of this function for what
transformations it actually performs."
  (setq worldspec (copy-sequence worldspec))
  (let ((logins (plist-get worldspec :logins)) (logins-modified nil))
    ;; Convert from single-login format to multi-login format.
    (when-let ((login (plist-get worldspec :login)))
      (setq logins (cons login logins)
            logins-modified t
            worldspec (impru--plist-delete-all worldspec :login)))
    ;; Convert from string login format to string+plist login format.
    (when (seq-some #'stringp logins)
      (setq logins (mapcar (lambda (login)
                             (if (stringp login) (cons login nil) login))
                           logins)
            logins-modified t))
    (when logins-modified
      (setq worldspec (plist-put (impru--plist-delete-all worldspec :logins)
                                 :logins logins))))
  ;; Convert from single-hostspec format to multi-hostspec format.
  (when-let ((host (plist-get worldspec :host)))
    (let ((hostspec (list :host (plist-get worldspec :host)
                          :port (plist-get worldspec :port)
                          :tls (plist-get worldspec :tls))))
      (setq worldspec (plist-put (impru--plist-delete-all worldspec :host :port :tls)
                             :hostspecs (cons hostspec (plist-get worldspec :hostspecs))))))
  worldspec)

(defun impru-in-place-upgrade-user-worlds (&optional show-messages)
  ;; XXX: doesn't handle buffers, but that might not be necessary anyway.
  (interactive (list t))
  (setq impru-user-world-alist
        (mapcar (lambda (entry)
                  (cons (car entry)
                        ;; XXX: warn if reimport fails?
                        (or (impru-import-worldspec (cdr entry))
                            (cdr entry))))
                impru-user-world-alist))
  (when show-messages
    (message "User worlds converted to latest format")))

(defun impru--read-world-index (file-name)
  "Read the world index file FILE-NAME, returning an alist or error symbol.
If FILE-NAME is non-nil, exists, and is read successfully, the
world alist read from it is returned. Unrecognized forms and
unreadable worldspecs are skipped. Other errors occurring while
reading FILE-NAME are propagated.

If FILE-NAME is nil or refers to a nonexistent file, the symbol
‘file-did-not-exist’ is returned."
  (if (null file-name) 'file-did-not-exist
    (let ((warnings '()) (alist '()))
      ;; TODO: actually do something with the warnings?
      (impru--do-eld-file-pcase (file-name)
        (`(,(or 'impru-world 'mnrcr-world) . ,plist)
         (if-let ((worldspec (impru-import-worldspec plist)))
             (push (cons (plist-get worldspec :name) worldspec) alist)
           (push (list "Could not import world: %S" plist) warnings)))
        (unrecognized
         (push (list "Unrecognized form: %S" unrecognized) warnings))
        (:after (cons 'list (nreverse alist)))
        (:if-does-not-exist 'file-did-not-exist)))))

(defun impru-load-user-worlds (&optional show-messages no-overwrite)
  "Load user world definitions from the world index file.
The definitions read become the new saved definitions, overwriting
‘impru-saved-user-world-alist’. They are also copied as the new
current definitions, overwriting ‘impru-user-world-alist’, unless
NO-OVERWRITE is non-nil, in which case they are not made current.

If SHOW-MESSAGES is non-nil, progress messages may be displayed."
  (interactive (list t nil))
  (let* ((directory (impru-get-user-world-directory))
         (file-name (and directory (concat directory impru-world-index-file-name))))
    (pcase-exhaustive (impru--read-world-index file-name)
      ('file-did-not-exist
       (prog1 nil
         (when show-messages
           (message "No world index file to load"))))
      (`(list . ,alist)
       (prog1 t
         (when show-messages
           (message "Loaded %d worlds" (length alist)))
         (setq impru-saved-user-world-alist alist)
         (unless no-overwrite
           (setq impru-user-world-alist (impru--copy-world-alist alist))))))))

(defun impru-revert-user-world (world-name)
  "Revert the user world definition for WORLD-NAME from its saved copy.
Any world index file is not reread; the saved definition for WORLD-NAME
is taken from ‘impru-saved-user-world-alist’."
  (impru-load-user-worlds nil t)
  (setf (map-elt impru-user-world-alist world-name)
        (copy-sequence (map-elt impru-saved-user-world-alist world-name))))

(defsubst impru--normalize-world-name (name)
  "Return the canonical form of the world name designated by NAME.
If NAME is a string, it is converted into a symbol."
  (if (symbolp name) name
    (intern (format "%s" name))))

(defun impru-read-world-name (&optional or-new)
  "Read a world name from the minibuffer, returning it as a string.
Completions are taken from the current set of user world
definitions. If OR-NEW is non-nil, unrecognized world
names are permitted as inputs; otherwise, they are not.
Return the world name."
  (let* ((completions-1 (mapcar (lambda (entry) (symbol-name (car entry))) impru-user-world-alist))
         (completions (sort completions-1 #'string<)))
    (impru--completing-read "World: " completions (not or-new))))

(defun impru-read-login-name (&optional world-name always-ask)
  "Read a login name from the minibuffer, returning it as a string.
If WORLD-NAME is non-nil, completions are taken from the
known set of logins for the user world named WORLD-NAME;
in that case, if there is only one login available, it
is returned immediately unless ALWAYS-ASK is also non-nil.
Unrecognized login names are always permitted as inputs."
  (setq world-name (impru--normalize-world-name world-name))
  (let* ((completions-1
          (and world-name
               (mapcar #'car
                (plist-get (impru--borrow-user-world world-name) :logins))))
         (completions (sort completions-1 #'string<)))
    (impru--completing-read "Login: " completions nil
                            :default 0 :immediate (not always-ask))))

(defun impru-read-login-name--prefix-arg (world-name prefix)
  "Read a login name from the minibuffer, varying behavior with PREFIX.
This is a helper function for ‘impru-connect-to-world’, which see."
  (when (or (null prefix) (impru--positive-prefix-arg-p prefix))
    (impru-read-login-name world-name (and prefix t))))

(defun impru-connspec-for-worldspec (worldspec &optional login-name)
  "Return a connspec suitable for connecting to WORLDSPEC as LOGIN-NAME.
LOGIN-NAME may be a string, or nil to indicate that no login
or login-specific configuration should be attempted."
  (let* ((hostspec (car (plist-get worldspec :hostspecs)))
         (password-ref nil))
    (when-let ((login-options (cdr (assoc login-name (plist-get worldspec :logins)))))
      (when-let ((got-password-ref (plist-get login-options :password-ref)))
        (setq password-ref got-password-ref)))
    (nconc
     (copy-sequence hostspec)
     (list :world-name (symbol-name (plist-get worldspec :name)))
     ;;XXX: the latter should really be world-name for consistency, and then
     ;; the former should be world-display-name
     (list :world-storage-name (plist-get worldspec :name))
     (and login-name (list :login login-name))
     (and password-ref (list :password-ref password-ref)))))

;;;###autoload
(defun impru-connect-to-world (world-name &optional login-name)
  "Open a connection to a configured text virtual world (MU*).
WORLD-NAME is a symbol or string representing a configured user
world. LOGIN-NAME, if provided, is a string representing what
avatar, character, or user to log in as.

When called interactively, the user is prompted for the world
name. The login name behavior depends on the prefix argument: a
positive prefix argument means to always prompt for a login name,
a nonpositive prefix argument means not to use a login name, and
no prefix argument means to use the configured login name without
prompting if the world is configured with one, or prompt if the
world is configured with more than one."
  (interactive
   (let* ((world-name (impru-read-world-name))
          (login-name (impru-read-login-name--prefix-arg world-name current-prefix-arg)))
     (list world-name login-name)))
  (setq world-name (impru--normalize-world-name world-name)
        login-name (if (and login-name (string-empty-p login-name)) nil login-name))
  (if-let ((worldspec (impru--borrow-user-world world-name)))
      (if-let ((connspec (impru-connspec-for-worldspec worldspec login-name)))
          (impru-open-connection connspec)
        ;; TODO: ... so what does the user do?
        (error "Not enough information to connect to %s"))
    (error "No such world: %s" world-name)))

;;;###autoload (autoload 'mnrcr-connect-to-world "impru")
(impru-defcompat mnrcr 'mnrcr-connect-to-world 'impru-connect-to-world)

(provide 'impru)

;;; impru.el ends here
